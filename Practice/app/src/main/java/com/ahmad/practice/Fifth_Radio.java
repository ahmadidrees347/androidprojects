package com.ahmad.practice;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.Toast;

public class Fifth_Radio extends AppCompatActivity {

    RadioButton rd_1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fifth_radio);
        
        rd_1=findViewById(R.id.radiobtn1);
        
        rd_1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    Toast.makeText(Fifth_Radio.this, "Checked", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void RadioClick(View view) {
        boolean isChecked = ((RadioButton) view).isChecked();

        switch (view.getId())
        {
            case R.id.radiobtn2:
            {
                if(isChecked)
                {
                    Toast.makeText(this, "Android", Toast.LENGTH_SHORT).show();
                }
                break;
            }
            case R.id.radiobtn3:
            {
                if(isChecked)
                {
                    Toast.makeText(this, "JAVA", Toast.LENGTH_SHORT).show();
                }
                break;
            }
        }
    }
}
