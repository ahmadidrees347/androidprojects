package com.ahmad.practice;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Fourth_Text extends AppCompatActivity implements View.OnClickListener {

    EditText edt1;
    EditText edt2;
    Button btn_edt_click;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fourth__text);

        edt1=findViewById(R.id.edt_1);
        edt2=findViewById(R.id.edt_2);
        btn_edt_click=findViewById(R.id.btn_edt);

        btn_edt_click.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(R.id.btn_edt==v.getId())
        {
            edt1.setText("My name is Ahmad.");
            String value = edt2.getText().toString();
            Toast.makeText(this, value, Toast.LENGTH_SHORT).show();
        }
    }
}
