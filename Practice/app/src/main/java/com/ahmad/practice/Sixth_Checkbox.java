package com.ahmad.practice;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.Toast;

public class Sixth_Checkbox extends AppCompatActivity {

    CheckBox chk1,chk2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sixth__checkbox);

        chk1=findViewById(R.id.chk_1);
        chk2=findViewById(R.id.chk_2);

        /*chk1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    Toast.makeText(Sixth_Checkbox.this, "Android Checked", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Toast.makeText(Sixth_Checkbox.this, "Android UnChecked", Toast.LENGTH_SHORT).show();
                }
            }
        });
        chk2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    Toast.makeText(Sixth_Checkbox.this, "JAVA Checked", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Toast.makeText(Sixth_Checkbox.this, "JAVA UnChecked", Toast.LENGTH_SHORT).show();
                }
            }
        });*/

    }

    public void checkClick(View view) {

        boolean isChecked = ((CheckBox) view).isChecked();

        switch (view.getId())
        {
            case R.id.chk_1:
            {
                if(isChecked)
                {
                    Toast.makeText(Sixth_Checkbox.this, "Android Checked", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Toast.makeText(Sixth_Checkbox.this, "Android UnChecked", Toast.LENGTH_SHORT).show();
                }
                break;
            }
            case R.id.chk_2:
            {
                if(isChecked)
                {
                    Toast.makeText(Sixth_Checkbox.this, "JAVA Checked", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Toast.makeText(Sixth_Checkbox.this, "JAVA UnChecked", Toast.LENGTH_SHORT).show();
                }
                break;
            }
        }
    }
}
