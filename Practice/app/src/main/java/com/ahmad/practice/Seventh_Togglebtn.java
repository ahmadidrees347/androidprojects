package com.ahmad.practice;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.Toast;
import android.widget.ToggleButton;

public class Seventh_Togglebtn extends AppCompatActivity {

    ToggleButton btn_toggle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seventh__togglebtn);

        btn_toggle=findViewById(R.id.togglebtn);

        btn_toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    Toast.makeText(Seventh_Togglebtn.this, "ON", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Toast.makeText(Seventh_Togglebtn.this, "OFF", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }
}
