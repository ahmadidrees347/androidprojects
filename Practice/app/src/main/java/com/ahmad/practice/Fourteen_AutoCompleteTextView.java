package com.ahmad.practice;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

public class Fourteen_AutoCompleteTextView extends AppCompatActivity {

    AutoCompleteTextView autoCompleteTextView;
    String[] programmingLang = {"Android","JAVA","C++","Php","Python"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fourteen__auto_complete_text_view);

        autoCompleteTextView = findViewById(R.id.btn_auto_complete_textView);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,R.layout.support_simple_spinner_dropdown_item,programmingLang);
        autoCompleteTextView.setAdapter(adapter);
        //After type 2 character suggestion come out
        autoCompleteTextView.setThreshold(1);

    }
}
