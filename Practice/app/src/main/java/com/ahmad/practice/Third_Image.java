package com.ahmad.practice;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class Third_Image extends AppCompatActivity implements View.OnClickListener{

    Button show;
    ImageView img;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third__image);

        show = findViewById(R.id.btnImg);
        img = findViewById(R.id.pic);
        show.setOnClickListener(this);
        img.setOnClickListener(this);
    }
    @Override
    public void onClick(View v) {
        String str= show.getText().toString();

        if(v.getId()==R.id.btnImg && str.equalsIgnoreCase("Show"))
        {
            img.setBackgroundResource(R.drawable.abc);
            show.setText("Hide");
        }
       else if(v.getId()==R.id.btnImg && str.equalsIgnoreCase("Hide"))
        {
            img.setBackgroundResource(0);
            show.setText("Show");
        }

    }
}
