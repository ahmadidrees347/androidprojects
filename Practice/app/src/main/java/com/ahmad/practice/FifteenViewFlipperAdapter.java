package com.ahmad.practice;

import android.content.Context;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.zip.Inflater;

public class FifteenViewFlipperAdapter extends BaseAdapter {

    private Context context;
    private String[] countryNames;
    private int[] flagsImage;
    private LayoutInflater layoutInflater;


    public FifteenViewFlipperAdapter(Context context, String[] countryNames, int[] flagsImage) {
        this.context = context;
        this.countryNames = countryNames;
        this.flagsImage = flagsImage;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {

        return countryNames.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = layoutInflater.inflate(R.layout.activity_fifteen_list_flipper_view,parent,false);
        ImageView imgFlags = convertView.findViewById(R.id.img_countryName);
        TextView txt_countryNames = convertView.findViewById(R.id.txt_countryName);

        imgFlags.setBackgroundResource(flagsImage[position]);
        txt_countryNames.setText(countryNames[position]);

        return convertView;
    }
}
