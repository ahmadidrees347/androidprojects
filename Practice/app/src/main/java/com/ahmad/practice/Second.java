package com.ahmad.practice;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class Second extends AppCompatActivity implements View.OnClickListener {

    Button btn_1;
    Button btn_2;
    Button btn_3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        btn_1=findViewById(R.id.btn_One);
        btn_2=findViewById(R.id.btn_Two);
        btn_3=findViewById(R.id.btn_Three);

        btn_1.setOnClickListener(this);
        btn_2.setOnClickListener(this);
        btn_3.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btn_One:
            {
                Toast.makeText(this, "Button One", Toast.LENGTH_SHORT).show();
                break;
            }
            case R.id.btn_Two:
            {
                Toast.makeText(this, "Button Two", Toast.LENGTH_SHORT).show();
                break;
            }
            case R.id.btn_Three:
            {
                Toast.makeText(this, "Button Three", Toast.LENGTH_SHORT).show();
                break;
            }
        }
    }
}
