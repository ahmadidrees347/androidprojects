package com.ahmad.practice;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.Calendar;

public class Twelveth_TimePicker extends AppCompatActivity {

    TimePicker timePicker;
    TextView txt_View;
    Calendar calendar;
    String format="";
    Button btn_click;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_twelveth__time_picker);

        timePicker = findViewById(R.id.btn_TimePicker);
        txt_View = findViewById(R.id.btn_Text);
        btn_click = findViewById(R.id.btn_PickTime);

        calendar = Calendar.getInstance();

        btn_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int hours = timePicker.getCurrentHour();
                int min = timePicker.getCurrentMinute();
                showTime(hours,min);
            }
        });
    }

    private void showTime(int hours, int minutes)
    {
        if(hours==0)
        {
            hours+=12;
            format="AM";
        }
        else if(hours==12)
        {
            format="PM";
        }
        else if(hours>12)
        {
            hours-=12;
            format="PM";
        }
        else
        {
            format="AM";
        }
        txt_View.setText(new StringBuilder().append(hours).append(":").append(minutes).append("").append(format));
    }
}
