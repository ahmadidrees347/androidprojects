package com.ahmad.practice;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ProgressBar;


public class Nineth_progressDialog extends AppCompatActivity {
    Button btn_Prog;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nineth_progress_dialog);

        btn_Prog = findViewById(R.id.btn_show_dialog_default);

        progressDialog = new ProgressDialog(this);
        //when user touch the Dialog cancel
        progressDialog.setCancelable(true);

        progressDialog.setTitle("Android Class");
        progressDialog.setMessage("This is Android class");
        progressDialog.show();
    }
}
