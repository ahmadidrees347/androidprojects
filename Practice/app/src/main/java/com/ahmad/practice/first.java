package com.ahmad.practice;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class first extends AppCompatActivity implements View.OnClickListener{

    Button btns;
    Button img;
    Button btn_txt;
    Button btn_radio;
    Button btn_checkbox;
    Button btn_tgl;
    Button btn_r_bar;
    Button btn_progresDialog;
    Button btn_Progress;
    Button btn_alert;
    Button btn_spn;
    Button btn_timePicker;
    Button btn_ProgressBar;
    Button btn_autoCompleteTextView;
    Button btn_AdapterViewFlipper;
    Button btn_SeekBar;
    Button btn_GridView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);

        btns=findViewById(R.id.Buttons);
        img=findViewById(R.id.Image);
        btn_txt=findViewById(R.id.btn_Text);
        btn_radio=findViewById(R.id.btn_Radio);
        btn_checkbox=findViewById(R.id.btn_chk);
        btn_tgl=findViewById(R.id.btn_tgl);
        btn_r_bar=findViewById(R.id.btn_ratingbar);
        btn_progresDialog=findViewById(R.id.btn_Progress);
        btn_alert=findViewById(R.id.btn_AlertDialog);
        btn_spn=findViewById(R.id.btn_Spinner);
        btn_timePicker=findViewById(R.id.btn_TimePicker);
        btn_ProgressBar=findViewById(R.id.btn_Progressbar);
        btn_autoCompleteTextView=findViewById(R.id.btn_autoCompleteTextView);
        btn_AdapterViewFlipper=findViewById(R.id.btn_AdapterViewFlipper);
        btn_SeekBar=findViewById(R.id.btn_SeekBar);
        btn_GridView=findViewById(R.id.btn_GridView);

        btns.setOnClickListener(this);
        img.setOnClickListener(this);
        btn_txt.setOnClickListener(this);
        btn_radio.setOnClickListener(this);
        btn_checkbox.setOnClickListener(this);
        btn_tgl.setOnClickListener(this);
        btn_r_bar.setOnClickListener(this);
        btn_progresDialog.setOnClickListener(this);
        btn_progresDialog.setOnClickListener(this);
        btn_alert.setOnClickListener(this);
        btn_spn.setOnClickListener(this);
        btn_timePicker.setOnClickListener(this);
        btn_ProgressBar.setOnClickListener(this);
        btn_autoCompleteTextView.setOnClickListener(this);
        btn_AdapterViewFlipper.setOnClickListener(this);
        btn_SeekBar.setOnClickListener(this);
        btn_GridView.setOnClickListener(this);

    }

    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.Buttons:
            {
                Intent second = new Intent(getApplicationContext(),Second.class);
                startActivity(second);
                break;
            }
            case R.id.Image:
            {
                Intent img = new Intent(getApplicationContext(),Third_Image.class);
                startActivity(img);
                break;
            }
            case R.id.btn_Text:
            {
                Intent txt = new Intent(getApplicationContext(),Fourth_Text.class);
                startActivity(txt);
                break;
            }
            case R.id.btn_Radio:
            {
                Intent radio = new Intent(getApplicationContext(),Fifth_Radio.class);
                startActivity(radio);
                break;
            }
            case R.id.btn_chk:
            {
                Intent checkboxActivity = new Intent(getApplicationContext(),Sixth_Checkbox.class);
                startActivity(checkboxActivity);
                break;
            }
            case R.id.btn_tgl:
            {
                Intent toggle = new Intent(getApplicationContext(),Seventh_Togglebtn.class);
                startActivity(toggle);
                break;
            }
            case R.id.btn_ratingbar:
            {
                Intent ratingbar = new Intent(getApplicationContext(),Eight_ratingBar.class);
                startActivity(ratingbar);
                break;
            }
            case R.id.btn_Progress:
            {
                Intent progressDialog = new Intent(getApplicationContext(),Nineth_progressDialog.class);
                startActivity(progressDialog);
                break;
            }
            case R.id.btn_AlertDialog:
            {
                Intent alertDialog = new Intent(getApplicationContext(),Tenth_AlertDialog.class);
                startActivity(alertDialog);
                break;
            }
            case R.id.btn_Spinner:
            {
                Intent spinner = new Intent(getApplicationContext(),Eleventh_Spinner.class);
                startActivity(spinner);
                break;
            }
            case R.id.btn_TimePicker:
            {
                Intent timePicker = new Intent(getApplicationContext(),Twelveth_TimePicker.class);
                startActivity(timePicker);
                break;
            }

            case R.id.btn_Progressbar:
            {
                Intent progressBar = new Intent(getApplicationContext(),Thirdteen_ProgressBar.class);
                startActivity(progressBar);
                break;
            }

            case R.id.btn_autoCompleteTextView:
            {
                Intent autoCompleteTextView = new Intent(getApplicationContext(),Fourteen_AutoCompleteTextView.class);
                startActivity(autoCompleteTextView);
                break;
            }

            case R.id.btn_AdapterViewFlipper:
            {
                Intent AdapterViewFlipper = new Intent(getApplicationContext(),Fifteen_AdapterViewFlipper.class);
                startActivity(AdapterViewFlipper);
                break;
            }

            case R.id.btn_SeekBar:
            {
                Intent SeekBar = new Intent(getApplicationContext(),Fifteen_SeekBar.class);
                startActivity(SeekBar);
                break;
            }

            case R.id.btn_GridView:
            {
                Intent GridView = new Intent(getApplicationContext(),Sixteen_GridView.class);
                startActivity(GridView);
                break;
            }
        }

    }
}
