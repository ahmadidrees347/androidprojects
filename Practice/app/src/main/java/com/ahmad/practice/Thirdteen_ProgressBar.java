package com.ahmad.practice;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ProgressBar;
import android.widget.TextView;

public class Thirdteen_ProgressBar extends AppCompatActivity {

    ProgressBar progressBar;
    int progressStatus;
    Handler handler = new Handler();
    TextView txt_progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thirdteen__progress_bar);

        progressBar = findViewById(R.id.progressbar);
        txt_progress = findViewById(R.id.txt_process);
        new Thread(new Runnable() {
            @Override
            public void run() {
                while(progressStatus<100)
                {
                    progressStatus+=1;

                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setProgress(progressStatus);
                            txt_progress.setText(progressStatus+ "/" + progressBar.getMax());
                        }
                    });

                    try
                    {
                        Thread.sleep(200);
                    }
                    catch (InterruptedException e)
                    {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

    }
}
