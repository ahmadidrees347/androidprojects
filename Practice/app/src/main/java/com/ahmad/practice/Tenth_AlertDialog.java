package com.ahmad.practice;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class Tenth_AlertDialog extends AppCompatActivity {

    Button btn_cust;
    Button btn_deaf;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tenth__alert_dialog);

        btn_cust=findViewById(R.id.btn_show_custom);
        btn_deaf=findViewById(R.id.btn_show_default);

        final AlertDialog[] alertDialog = {null};
        btn_cust.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadCustom();
            }
        });

        btn_deaf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(Tenth_AlertDialog.this);
                builder.setCancelable(false);
                builder.setTitle("Android");
                builder.setMessage("Are you Sure?");
                
                builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(Tenth_AlertDialog.this, "YES", Toast.LENGTH_SHORT).show();
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(Tenth_AlertDialog.this, "Cancelled", Toast.LENGTH_SHORT).show();
                    }
                });

                alertDialog[0] = builder.create();
                alertDialog[0].show();
                Toast.makeText(Tenth_AlertDialog.this, "Canceled", Toast.LENGTH_SHORT).show();
            }
        });



    }
    private void loadCustom()
    {
        AlertDialog.Builder bull = new AlertDialog.Builder(Tenth_AlertDialog.this);
        LayoutInflater inflater = this.getLayoutInflater();

        final View customDialogView = inflater.inflate(R.layout.activity_tenth_custom_alert,null);
        bull.setView(customDialogView);
        bull.setTitle("Custom View");
        bull.setCancelable(false);
        final AlertDialog alertDialog1 = bull.create();
        final Button btn_ok = customDialogView.findViewById(R.id.btn_ok);
        final Button btn_cancel = customDialogView.findViewById(R.id.btn_Cancel);

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog1.dismiss();
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog1.dismiss();
            }
        });
        alertDialog1.show();
    }
}
