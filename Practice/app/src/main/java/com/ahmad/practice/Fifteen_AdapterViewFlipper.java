package com.ahmad.practice;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterViewFlipper;
import android.widget.Button;

public class Fifteen_AdapterViewFlipper extends AppCompatActivity implements View.OnClickListener {

    Button btn_Next;
    Button btn_Prev;

    AdapterViewFlipper adapterViewFlipper;
    String[] countryNames = {"Pakistan","China","KSA","Malaysia"};
    int[] flags = {R.drawable.pakistan,R.drawable.china,R.drawable.saudia_arabia,R.drawable.malaysia};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fifteen__adapter_view_flipper);

        adapterViewFlipper = findViewById(R.id.adapterflipper);

        FifteenViewFlipperAdapter adapter = new FifteenViewFlipperAdapter(this,countryNames,flags);

        adapterViewFlipper.setAdapter(adapter);

        //adapterViewFlipper.setFlipInterval(300);//300 milli seconds
        //adapterViewFlipper.setAutoStart(true);

        btn_Next=findViewById(R.id.btn_next);
        btn_Prev=findViewById(R.id.btn_previous);

        btn_Prev.setOnClickListener(this);
        btn_Next.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btn_next:
            {
                adapterViewFlipper.showNext();
                break;
            }
            case R.id.btn_previous:
            {
                adapterViewFlipper.showPrevious();
                break;
            }
        }

    }
}
