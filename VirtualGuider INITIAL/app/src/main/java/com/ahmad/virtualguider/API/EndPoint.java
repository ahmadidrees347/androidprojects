package com.ahmad.virtualguider.API;

public class EndPoint {
    public static final String ROOT_URL = "http://192.168.1.18:8012/VG/API/";
    public static final String SHOW_TEACHERNAME = ROOT_URL + "SelectTeacher.php";
    public static final String SHOW_STATUS = ROOT_URL + "SelectTeacherStatus.php";
    public static final String SHOW_SCHEDULE = ROOT_URL + "SelectSchedule.php";
    public static final String SHOW_TIMETABLE = ROOT_URL + "SelectTimetable.php";
}
