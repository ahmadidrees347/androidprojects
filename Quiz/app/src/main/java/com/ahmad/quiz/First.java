package com.ahmad.quiz;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class First extends AppCompatActivity implements View.OnClickListener {
    Button btn_islam;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);

        btn_islam = findViewById(R.id.btn_islamic);

        btn_islam.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btn_islamic:
            {
                Intent second_Islam = new Intent(getApplicationContext(),Second_Islamic.class);
                startActivity(second_Islam);
                break;
            }
        }
    }
}
