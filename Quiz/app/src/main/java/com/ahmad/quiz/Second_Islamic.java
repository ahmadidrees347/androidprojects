package com.ahmad.quiz;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class Second_Islamic extends AppCompatActivity implements View.OnClickListener {

    TextView t1[] = new TextView[10];
    ArrayList <String> questions = new ArrayList<String>();
    boolean flag[] = new boolean[5];
    Button btn_Submit;
    int count;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        count=0;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second__islamic);

        questions.add("What do we call the Angels who write down what we do ?");
        questions.add("Pillars of Islam are also called ...…");
        questions.add("A prophet is called .......... in Arabic.");
        questions.add("What does Islam mean ?");
        questions.add("Allah (s.w.t.) has ......…");
        questions.add("How many Allah (s.w.t.)'s are there ?");
        questions.add("Who made the sun, the moon, the stars, the sky, the hills, the rivers, the flowers etc.?");
        questions.add("What do you say when you sneeze ?");
        questions.add("What is the first month of the Islamic Calendar?");
        questions.add("What is Salat?");


        for(int i=0;i<5;i++)
        {
            flag[i]=false;
        }

        t1[0]=findViewById(R.id.q1);
        t1[1]=findViewById(R.id.q2);
        t1[2]=findViewById(R.id.q3);
        t1[3]=findViewById(R.id.q4);
        t1[4]=findViewById(R.id.q5);
        t1[5]=findViewById(R.id.q6);
        t1[6]=findViewById(R.id.q7);
        t1[7]=findViewById(R.id.q8);
        t1[8]=findViewById(R.id.q9);
        t1[9]=findViewById(R.id.q10);

        for (int i=0;i<10;i++)
        {
            t1[i].setText(questions.get(i));
        }

        btn_Submit = findViewById(R.id.btn_Submit);

        btn_Submit.setOnClickListener(this);
    }


    public void RadioClick(View view) {
        boolean isChecked;


        switch (view.getId())
        {
            case R.id.q1_1:
            {
                isChecked = ((RadioButton) view).isChecked();
                if(isChecked && flag[0]==true)
                {
                    count--;
                    flag[0]=false;
                }
                break;
            }
            case R.id.q1_2:
            {
                isChecked = ((RadioButton) view).isChecked();
                if(isChecked && flag[0]==false)
                {
                    flag[0]=true;
                    count++;
                }
                break;
            }
            case R.id.q1_3:
            {
                isChecked = ((RadioButton) view).isChecked();
                if(isChecked && flag[0]==true)
                {
                    count--;
                    flag[0]=false;
                }
                break;
            }
            case R.id.q1_4:
            {
                isChecked = ((RadioButton) view).isChecked();
                if(isChecked && flag[0]==true)
                {
                    count--;
                    flag[0]=false;
                }
                break;
            }
            case R.id.q2_1:
            {
                isChecked = ((RadioButton) view).isChecked();
                if(isChecked && flag[1]==true)
                {
                    count--;
                    flag[1]=false;
                }
                break;
            }
            case R.id.q2_2:
            {
                isChecked = ((RadioButton) view).isChecked();
                if(isChecked && flag[1]==false)
                {
                    flag[1]=true;
                    count++;
                }
                break;
            }
            case R.id.q2_3:
            {
                isChecked = ((RadioButton) view).isChecked();
                if(isChecked && flag[1]==true)
                {
                    count--;
                    flag[1]=false;
                }
                break;
            }
            case R.id.q2_4:
            {
                isChecked = ((RadioButton) view).isChecked();
                if(isChecked && flag[1]==true)
                {
                    count--;
                    flag[1]=false;
                }
                break;
            }

            case R.id.q3_1:
            {
                isChecked = ((RadioButton) view).isChecked();
                if(isChecked && flag[2]==false)
                {
                    flag[2]=true;
                    count++;
                }
                break;
            }
            case R.id.q3_2:
            {
                isChecked = ((RadioButton) view).isChecked();
                if(isChecked && flag[2]==true)
                {
                    count--;
                    flag[2]=false;
                }
                break;
            }
            case R.id.q3_3:
            {
                isChecked = ((RadioButton) view).isChecked();
                if(isChecked && flag[2]==true)
                {
                    count--;
                    flag[2]=false;
                }
                break;
            }
            case R.id.q3_4:
            {
                isChecked = ((RadioButton) view).isChecked();
                if(isChecked && flag[2]==true)
                {
                    count--;
                    flag[2]=false;
                }
                break;
            }

            case R.id.q4_1:
            {
                isChecked = ((RadioButton) view).isChecked();
                if(isChecked && flag[3]==false)
                {
                    flag[3]=true;
                    count++;
                }
                break;
            }
            case R.id.q4_2:
            {
                isChecked = ((RadioButton) view).isChecked();
                if(isChecked && flag[3]==true)
                {
                    count--;
                    flag[3]=false;
                }
                break;
            }
            case R.id.q4_3:
            {
                isChecked = ((RadioButton) view).isChecked();
                if(isChecked && flag[3]==true)
                {
                    count--;
                    flag[3]=false;
                }
                break;
            }
            case R.id.q4_4:
            {
                isChecked = ((RadioButton) view).isChecked();
                if(isChecked && flag[3]==true)
                {
                    count--;
                    flag[3]=false;
                }
                break;
            }

            case R.id.q5_1:
            {
                isChecked = ((RadioButton) view).isChecked();
                if(isChecked && flag[4]==true)
                {
                    count--;
                    flag[4]=false;
                }
                break;
            }
            case R.id.q5_2:
            {
                isChecked = ((RadioButton) view).isChecked();
                if(isChecked && flag[4]==true)
                {
                    count--;
                    flag[4]=false;
                }
                break;
            }
            case R.id.q5_3:
            {
                isChecked = ((RadioButton) view).isChecked();
                if(isChecked && flag[4]==false)
                {
                    flag[4]=true;
                    count++;
                }
                break;
            }
            case R.id.q5_4:
            {
                isChecked = ((RadioButton) view).isChecked();
                if(isChecked && flag[4]==true)
                {
                    count--;
                    flag[4]=false;
                }
                break;
            }
        }
    }
    @Override
    public void onClick(View v) {

        String s;
        s=Integer.toString(count);
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();

        /*switch (v.getId())
        {
            case R.id.btn_Submit:
            {
                String s;
                s=Integer.toString(count);
                Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
            }

        }*/


    }
}
