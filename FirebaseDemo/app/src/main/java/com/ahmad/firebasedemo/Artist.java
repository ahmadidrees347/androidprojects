package com.ahmad.firebasedemo;

public class Artist {
    String id;
    String Artist_Name;
    String Artist_Genre;

    public Artist() {}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getArtist_Name() {
        return Artist_Name;
    }

    public void setArtist_Name(String artist_Name) {
        Artist_Name = artist_Name;
    }

    public String getArtist_Genre() {
        return Artist_Genre;
    }

    public void setArtist_Genre(String artist_Genre) {
        Artist_Genre = artist_Genre;
    }

    public Artist(String id, String artist_Name, String artist_Genre) {

        this.id = id;
        this.Artist_Name = artist_Name;
        this.Artist_Genre = artist_Genre;
    }
}
