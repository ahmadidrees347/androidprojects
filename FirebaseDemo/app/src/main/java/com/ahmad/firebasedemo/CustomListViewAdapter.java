package com.ahmad.firebasedemo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

public class CustomListViewAdapter extends BaseAdapter {

    Context context;
    private List<Artist> artistList;
    private LayoutInflater inflater;

     CustomListViewAdapter(Context context, List<Artist> artistList) {
        this.context = context;
        this.artistList = artistList;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return artistList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final Artist artist = artistList.get(position);
        convertView = inflater.inflate(R.layout.activity_artist_list, parent, false);

        TextView txt_id = convertView.findViewById(R.id.artist_id);
        TextView txt_name = convertView.findViewById(R.id.artist_name);
        TextView txt_genre = convertView.findViewById(R.id.artist_genre);

        txt_id.setText(artist.getId());
        txt_name.setText(artist.getArtist_Name());
        txt_genre.setText(artist.getArtist_Genre());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, artist.getArtist_Name(), Toast.LENGTH_SHORT).show();
                DatabaseReference artistdelref= FirebaseDatabase.getInstance().getReference("Artist").child(artist.getId());
                artistdelref.removeValue();
            }
        });

        return convertView;
    }
}
