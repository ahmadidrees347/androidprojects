package com.ahmad.firebasedemo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button btn_insert, btn_delete, btn_update, btn_display;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn_insert = findViewById(R.id.btn_insert);
        btn_delete = findViewById(R.id.btn_delete);
        btn_update = findViewById(R.id.btn_update);
        btn_display = findViewById(R.id.btn_display);

        btn_insert.setOnClickListener(this);
        btn_delete.setOnClickListener(this);
        btn_update.setOnClickListener(this);
        btn_display.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.btn_insert:
            {
                Intent insert = new Intent(getApplicationContext(),Insert.class);
                startActivity(insert);
                break;
            }
            case R.id.btn_delete:
            {
                Intent delete = new Intent(getApplicationContext(),Delete.class);
                startActivity(delete);
                break;
            }
            case R.id.btn_update:
            {
                Intent update = new Intent(getApplicationContext(),Update.class);
                startActivity(update);
                break;
            }
            case R.id.btn_display:
            {
                Intent display = new Intent(getApplicationContext(),Display.class);
                startActivity(display);
                break;
            }
        }

    }
}
