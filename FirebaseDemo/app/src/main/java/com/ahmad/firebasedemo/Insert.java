package com.ahmad.firebasedemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class Insert extends AppCompatActivity {
    FirebaseDatabase database;
    DatabaseReference myRef;
    EditText edt_Name, edt_Genre;
    Button btn_Save;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert);

        edt_Name = findViewById(R.id.edt_name);
        edt_Genre =findViewById(R.id.edt_genre);

        btn_Save = findViewById(R.id.btn_save);

        myRef = FirebaseDatabase.getInstance().getReference("Artist");

        btn_Save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addRecord();
            }
        });
    }
    private void addRecord()
    {
        String Artist_Name = edt_Name.getText().toString().trim();
        String Artist_Genre = edt_Genre.getText().toString().trim();

        String id = myRef.push().getKey();
        Artist artist = new Artist(id,Artist_Name,Artist_Genre);
        myRef.child(id).setValue(artist);
    }
}
