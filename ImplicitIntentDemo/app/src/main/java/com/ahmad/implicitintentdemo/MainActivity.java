package com.ahmad.implicitintentdemo;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button btn_cam, btn_phone, btn_email;
    Intent intent;
    ImageView imageView;
    private static final int CAMERA_REQUEST = 12;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn_cam = findViewById(R.id.btn_Camera);
        btn_phone = findViewById(R.id.btn_phonedailer);
        btn_email = findViewById(R.id.btn_Email);
        imageView = findViewById(R.id.image);

        btn_cam.setOnClickListener(this);
        btn_phone.setOnClickListener(this);
        btn_email.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_Camera: {
                opencamera();
                break;
            }
            case R.id.btn_phonedailer: {
                calling();
                break;
            }
            case R.id.btn_Email: {
                openEmail();
                break;
            }
        }
    }

    private void opencamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {

                Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                imageView.setImageBitmap(bitmap);
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void calling() {
        intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:+923315309954"));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                reguestPermission();
            } else {
                startActivity(intent);
            }
        } else {
            startActivity(intent);
        }
    }

    private void reguestPermission() {
        ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.CALL_PHONE}, 100);
    }

    private void openEmail() {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setData(Uri.parse("email"));
        String emails[] = {"abc@gmail.com", "xyz@gmail.com"};
        intent.putExtra(Intent.EXTRA_EMAIL, emails);
        intent.putExtra(Intent.EXTRA_SUBJECT, "Implicit Intent");
        intent.putExtra(Intent.EXTRA_TEXT, "This is Android Intent");
        intent.setType("message/rfc822");
        Intent chooser = Intent.createChooser(intent, "Launch Email");
        startActivity(chooser);
    }
}
