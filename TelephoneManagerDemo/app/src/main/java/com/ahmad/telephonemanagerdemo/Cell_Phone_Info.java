package com.ahmad.telephonemanagerdemo;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class Cell_Phone_Info extends AppCompatActivity {

    private static final String TODO = "OK";
    TextView txt_phoneType, txt_imei, txt_subID, txt_simSerial,
            txt_netCountry, txt_Sim_ISO, txt_softwareVersion, txt_voicemail;
    Button btn_show_detail;
    String phoneType, imei, subID, simSerial, netCountry,
            Sim_ISO, softwareVersion, voicemail;
    private static final int PHONE_READ_REQUEST_CODE = 123;
    TelephonyManager manager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cell__phone__info);

        btn_show_detail = findViewById(R.id.btn_phone_detail);

        txt_phoneType = findViewById(R.id.tv_phone_type);
        txt_imei = findViewById(R.id.tv_imei);
        txt_subID = findViewById(R.id.tv_subscriber_id);
        txt_simSerial = findViewById(R.id.tv_sim_serialNo);
        txt_netCountry = findViewById(R.id.tv_networkCountry);
        txt_Sim_ISO = findViewById(R.id.tv_sim_countryIso);
        txt_softwareVersion = findViewById(R.id.tv_software_version);
        txt_voicemail = findViewById(R.id.tv_voicemail_no);


        btn_show_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int permisonCheck = ContextCompat.checkSelfPermission(
                        getApplicationContext(), Manifest.permission.READ_PHONE_STATE);

                if (permisonCheck == PackageManager.PERMISSION_GRANTED) {
                    getPhoneManager();
                } else {
                    ActivityCompat.requestPermissions(Cell_Phone_Info.this,
                            new String[]{Manifest.permission.READ_PHONE_STATE},
                            PHONE_READ_REQUEST_CODE);
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PHONE_READ_REQUEST_CODE: {
                if (grantResults.length > 0 &&
                        grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getPhoneManager();
                } else {
                    Toast.makeText(this, "You Don't Have Permision", Toast.LENGTH_SHORT).show();
                }
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @SuppressLint("MissingPermission")
    private void getPhoneManager() {
        TelephonyManager manager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

        int phonetype = manager.getPhoneType();
        switch (phonetype) {
            case (TelephonyManager.PHONE_TYPE_CDMA): {
                phoneType = "CDMA";
                break;
            }
            case (TelephonyManager.PHONE_TYPE_GSM): {
                phoneType = "GSM";
                break;
            }
            case (TelephonyManager.PHONE_TYPE_SIP): {
                phoneType = "SIP";
                break;
            }
            case (TelephonyManager.PHONE_TYPE_NONE): {
                phoneType = "NONE";
                break;
            }
        }

        imei = getIMEI(this);

        subID = manager.getSubscriberId();
        simSerial = manager.getSimSerialNumber();
        netCountry = manager.getNetworkCountryIso();
        Sim_ISO = manager.getSimCountryIso();
        softwareVersion = manager.getDeviceSoftwareVersion();
        voicemail = manager.getVoiceMailNumber();

        txt_phoneType.setText("Phone Type :" + phoneType);
        txt_imei.setText("IMEI No :" + imei);
        txt_subID.setText("subID No :" + subID);
        txt_simSerial.setText("simSerial No :" + simSerial);
        txt_netCountry.setText("netCountry No :" + netCountry);
        txt_Sim_ISO.setText("Sim_ISO No :" + Sim_ISO);
        txt_softwareVersion.setText("softwareVersion No :" + softwareVersion);
        txt_voicemail.setText("voicemail No :" + voicemail);

    }

    public String getIMEI(Activity activity) {
        TelephonyManager telephonyManager = (TelephonyManager) activity
                .getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return TODO;
        }
        return telephonyManager.getDeviceId();
    }
}
