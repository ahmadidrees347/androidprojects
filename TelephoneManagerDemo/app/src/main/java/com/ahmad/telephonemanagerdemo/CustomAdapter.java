package com.ahmad.telephonemanagerdemo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class CustomAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private List<String> nameList;
    private List<String> numberList;
    Context context;

    CustomAdapter(List<String> nameList, List<String> numberList, Context context) {
        this.nameList = nameList;
        this.numberList = numberList;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return nameList.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        view  = inflater.inflate(R.layout.row_list_contacts,viewGroup,false);
        TextView tv_name  = view.findViewById(R.id.tv_name);
        TextView tv_contacts = view.findViewById(R.id.tv_phone_number);
        tv_name.setText(nameList.get(i));
        tv_contacts.setText(numberList.get(i));
        return view;
    }
}
