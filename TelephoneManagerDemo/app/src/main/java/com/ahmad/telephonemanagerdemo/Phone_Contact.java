package com.ahmad.telephonemanagerdemo;

import android.Manifest;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class Phone_Contact extends AppCompatActivity {

    private List<String> nameList = new ArrayList<>();
    private List<String> numberList = new ArrayList<>();
    CustomAdapter adapter;
    private static final int REQUEST_READ_CONTACTS = 123;
    ListView listView;
    Cursor cursor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone__contact);

        listView = findViewById(R.id.listview);
        int checkPermission = ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            if (checkPermission == PackageManager.PERMISSION_GRANTED) {
                readContacts();

            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_CONTACTS},
                        REQUEST_READ_CONTACTS);
            }
        } else {
            readContacts();
            adapter = new CustomAdapter(nameList, numberList, getApplicationContext());
            listView.setAdapter(adapter);
        }


    }

    private void readContacts() {

        cursor = getContentResolver().query(
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                null,
                null,
                null,
                ContactsContract.Contacts.DISPLAY_NAME + " ASC");

        if (cursor != null) {
            while (cursor.moveToNext()) {
                String contactName = cursor.getString(
                        cursor.getColumnIndex(
                                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                nameList.add(contactName);
                String contactsNumber = cursor.getString(
                        cursor.getColumnIndex(
                                ContactsContract.CommonDataKinds.Phone.NUMBER));
                numberList.add(contactsNumber);
            }

            cursor.close();
        }

        CustomAdapter adapter = new CustomAdapter(nameList, numberList, getApplicationContext());
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == REQUEST_READ_CONTACTS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Permission acepted!", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "get Permission first!!", Toast.LENGTH_SHORT).show();
            }

        }
    }

}

