package com.ahmad.telephonemanagerdemo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button btn_cellphone, btn_phoneContact, btn_readMsg, btn_sentMsg;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn_cellphone = findViewById(R.id.cellphone);
        btn_phoneContact = findViewById(R.id.phonecontacts);
        btn_readMsg = findViewById(R.id.readmessages);
        btn_sentMsg = findViewById(R.id.sendMessages);

        btn_cellphone.setOnClickListener(this);
        btn_phoneContact.setOnClickListener(this);
        btn_readMsg.setOnClickListener(this);
        btn_sentMsg.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.cellphone:
            {
                Intent intent = new Intent(getApplicationContext(),Cell_Phone_Info.class);
                startActivity(intent);
                break;
            }
            case R.id.phonecontacts:
            {
                Intent intent = new Intent(getApplicationContext(),Phone_Contact.class);
                startActivity(intent);
                break;
            }
            case R.id.readmessages:
            {
                Intent intent = new Intent(getApplicationContext(),Read_Messages.class);
                startActivity(intent);
                break;
            }
            case R.id.sendMessages:
            {
                Intent intent = new Intent(getApplicationContext(),Send_Messages.class);
                startActivity(intent);
                break;
            }
        }
    }
}
