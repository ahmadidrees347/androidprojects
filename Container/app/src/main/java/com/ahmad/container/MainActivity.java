package com.ahmad.container;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    ListView listView;

    int[] images={R.drawable.avatar_1_raster,R.drawable.avatar_2_raster
            ,R.drawable.avatar_3_raster,R.drawable.avatar_4_raster
            ,R.drawable.avatar_5_raster,R.drawable.avatar_6_raster
            ,R.drawable.avatar_7_raster,R.drawable.avatar_8_raster
            ,R.drawable.avatar_9_raster,R.drawable.avatar_10_raster
            ,R.drawable.avatar_11_raster,R.drawable.avatar_12_raster};
    String[] names= {"A","B","C","D","E","F","G","H","I","J","K","L"};
    List <DummyData> dummyDataList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView= findViewById(R.id.listview);
        setData();

        CustomListViewAdapter adapter = new CustomListViewAdapter(this,dummyDataList);
        listView.setAdapter(adapter);
    }



    private void setData() {
        dummyDataList = new ArrayList<>();
        for(int i=0;i<names.length-1;i++)
        {
            DummyData data = new DummyData(images[i],names[i]);
            dummyDataList.add(data);
        }
    }
}
