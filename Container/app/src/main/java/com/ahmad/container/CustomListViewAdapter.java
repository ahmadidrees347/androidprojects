package com.ahmad.container;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.List;


public class CustomListViewAdapter extends BaseAdapter {

    private Context context;
    private List<DummyData> dummyDataList;
    private LayoutInflater inflater;


    public CustomListViewAdapter(Context context, List<DummyData> dummyDataList) {
        this.context = context;
        this.dummyDataList = dummyDataList;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return dummyDataList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final DummyData dummyData = dummyDataList.get(position);
        convertView = inflater.inflate(R.layout.activity_list_row,parent,false);
        ImageView imageView = convertView.findViewById(R.id.img1);
        TextView textView = convertView.findViewById(R.id.txt_names);
        ImageView imageDel = convertView.findViewById(R.id.imgDel);

        Picasso.get().load(dummyData.getResID()).into(imageView);
        textView.setText(dummyData.getNames());


        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, dummyData.getNames(), Toast.LENGTH_SHORT).show();
            }
        });
        
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Image Click", Toast.LENGTH_SHORT).show();
            }
        });

        final AlertDialog[] alertDialog = {null};
        imageDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setCancelable(false);
                builder.setTitle("Android");
                builder.setMessage("Are you Sure?");

                builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dummyDataList.remove(position);
                        notifyDataSetChanged();
                        //Toast.makeText(context, "YES", Toast.LENGTH_SHORT).show();
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Toast.makeText(context, "Cancelled", Toast.LENGTH_SHORT).show();
                    }
                });

                alertDialog[0] = builder.create();
                alertDialog[0].show();
                //Toast.makeText(context, "Canceled", Toast.LENGTH_SHORT).show();
            }
        });
        return convertView;
    }
}
