package com.ahmad.container;

public class DummyData {

    private int resID;
    private String names;

    public int getResID() {
        return resID;
    }

    public void setResID(int resID) {
        this.resID = resID;
    }

    public String getNames() {
        return names;
    }

    public void setNames(String names) {
        this.names = names;
    }

    public DummyData(int resID, String names) {

        this.resID = resID;
        this.names = names;
    }
}
