package com.ahmad.attendence;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class Teacher_ApproveReq extends AppCompatActivity {

    ListView listView;
    List<Teacher> teacherList;
    SqliteDatabaseHelper sqliteDatabaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher__approve_req);

        listView = findViewById(R.id.TeacherApprovallist);

        teacherList = new ArrayList<>();
        sqliteDatabaseHelper = new SqliteDatabaseHelper(this);
        teacherList = sqliteDatabaseHelper.showAllRecordsofTeacher();
        //Log.d("rsult",studentList.toString());
        CustomListViewAdapterTeacher adapter = new CustomListViewAdapterTeacher(this,teacherList);
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }
}
