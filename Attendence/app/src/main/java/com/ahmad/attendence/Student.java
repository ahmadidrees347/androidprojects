package com.ahmad.attendence;


public class Student {
    int id;
    String rollno;
    String name;

    public Student(int id, String rollno, String name, String degree, String semester, String section, String department, String shift) {
        this.id = id;
        this.rollno = rollno;
        this.name = name;
        this.degree = degree;
        this.semester = semester;
        this.section = section;
        this.department = department;
        this.shift = shift;
    }

    public Student(String rollno, String name, String degree, String semester, String section, String department, String shift) {
        this.rollno = rollno;
        this.name = name;
        this.degree = degree;
        this.semester = semester;
        this.section = section;
        this.department = department;
        this.shift = shift;
    }

    String degree;
    String semester;
    String section;
    String department;
    String shift;

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public String getShift() {
        return shift;
    }

    public void setShift(String shift) {
        this.shift = shift;
    }

    public int getId() {

        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Student()
    {    }

    public String  getRollno() {
        return rollno;
    }

    public void setRollno(String rollno) {
        this.rollno = rollno;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }
}

