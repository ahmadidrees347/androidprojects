package com.ahmad.attendence;

public class Teacher {
    int id;
    String name;
    String cnic;
    String email;
    String pswd;

    public Teacher(String name, String cnic, String email, String pswd) {
        this.name = name;
        this.cnic = cnic;
        this.email = email;
        this.pswd = pswd;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Teacher() {
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCnic() {
        return cnic;
    }

    public void setCnic(String cnic) {
        this.cnic = cnic;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPswd() {
        return pswd;
    }

    public void setPswd(String pswd) {
        this.pswd = pswd;
    }

    public Teacher(int id, String name, String cnic, String email, String pswd) {

        this.id = id;
        this.name = name;
        this.cnic = cnic;
        this.email = email;
        this.pswd = pswd;

    }
}
