package com.ahmad.attendence;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

public class AdminDashBoard extends AppCompatActivity {

    ImageView img_STD, img_TEACH;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_dash_board);

        img_STD = findViewById(R.id.img_student);
        img_TEACH = findViewById(R.id.img_teacher);

        img_STD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent stdReg = new Intent(getApplicationContext(), StudentReg.class);
                startActivity(stdReg);
            }
        });

        img_TEACH.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent TeacherApproveReq = new Intent(getApplicationContext(), Teacher_ApproveReq.class);
                startActivity(TeacherApproveReq);
            }
        });
    }
}
