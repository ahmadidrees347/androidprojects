package com.ahmad.attendence;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Teacher_Registeration extends AppCompatActivity {

    EditText edt_name, edt_email, edt_pswd, edt_Cpswd, edt_cnic;
    Button T_signup;
    SqliteDatabaseHelper helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher__registeration);

        edt_name = findViewById(R.id.edt_name);
        edt_cnic = findViewById(R.id.edt_cnic);
        edt_email = findViewById(R.id.edt_email);
        edt_pswd = findViewById(R.id.edt_password);
        edt_Cpswd = findViewById(R.id.edt_Confirm_password);

        T_signup = findViewById(R.id.teacher_Signup);

        T_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String name = edt_name.getText().toString();
                String cnic = edt_cnic.getText().toString();
                String email = edt_email.getText().toString();
                String pswd = edt_pswd.getText().toString();
                String Confirm_pswd = edt_Cpswd.getText().toString();

                if(pswd.equals(Confirm_pswd))
                {
                    helper = new SqliteDatabaseHelper(getApplicationContext());
                    helper.TeacherDummyinsertData(getApplicationContext(),new Teacher(name,cnic,email,pswd));
                }
                else
                {
                    Toast.makeText(Teacher_Registeration.this, "PassWord Not Match", Toast.LENGTH_SHORT).show();
                    edt_name.setText("");
                    edt_cnic.setText("");
                    edt_email.setText("");
                    edt_pswd.setText("");
                    edt_Cpswd.setText("");
                }

            }
        });
    }
}
