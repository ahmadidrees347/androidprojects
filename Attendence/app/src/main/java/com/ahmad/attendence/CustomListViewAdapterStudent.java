package com.ahmad.attendence;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class CustomListViewAdapterStudent extends BaseAdapter {

    private Context context;
    private List<Student> studentList;
    private LayoutInflater inflater;


    public CustomListViewAdapterStudent(Context context, List<Student> studentList) {
        this.context = context;
        this.studentList = studentList;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount()
    {
        return studentList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final Student student = studentList.get(position);
        convertView = inflater.inflate(R.layout.activity_studentlist,parent,false);

        TextView txt_id = convertView.findViewById(R.id.s_id);
        TextView txt_name = convertView.findViewById(R.id.s_name);
        TextView txt_degree = convertView.findViewById(R.id.s_degree);
        TextView txt_rollno = convertView.findViewById(R.id.s_rollNo);
        TextView txt_semester = convertView.findViewById(R.id.s_semester);
        TextView txt_section = convertView.findViewById(R.id.s_section);
        TextView txt_dept = convertView.findViewById(R.id.s_dept);
        TextView txt_shift = convertView.findViewById(R.id.s_shift);

        txt_id.setText(String.valueOf(student.getId()));
        txt_name.setText(student.getName());
        txt_degree.setText(student.getDegree());
        txt_rollno.setText(student.getRollno());
        txt_semester.setText(student.getSemester());
        txt_section.setText(student.getSection());
        txt_dept.setText(student.getDepartment());
        txt_shift.setText(student.getShift());

        return convertView;
    }
}

