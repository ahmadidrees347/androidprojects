package com.ahmad.attendence;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class StudentReg extends AppCompatActivity {

    Spinner spinner_shift, spinner_section, spinner_dept, spinner_degree, spinner_semester;

    EditText edt_rollno,edt_name;
    String rollNo, name, semester, section, department, shift, degree;
    Button btn_add;
    SqliteDatabaseHelper helper;
    Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_reg);
        
        //EditText
        edt_rollno = findViewById(R.id.rollNo);
        edt_name = findViewById(R.id.name);

        //Spinner (DropDown)
        spinner_degree = findViewById(R.id.degree);
        spinner_semester = findViewById(R.id.semester);
        spinner_section = findViewById(R.id.section);
        spinner_dept = findViewById(R.id.department);
        spinner_shift =findViewById(R.id.spn_shift);
        //Button
        btn_add = findViewById(R.id.std_reg);

        ArrayAdapter<CharSequence> dummyAdapterDegree = ArrayAdapter.createFromResource(this,R.array.Degree,R.layout.support_simple_spinner_dropdown_item);
        dummyAdapterDegree.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinner_degree.setAdapter(dummyAdapterDegree);

        spinner_degree.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                degree = parent.getItemAtPosition(position).toString();
                if(degree.equalsIgnoreCase("BS"))
                {
                    //SEMSTER
                    ArrayAdapter<CharSequence> dummyAdapterSemester = ArrayAdapter.createFromResource(context, R.array.Semester8, R.layout.support_simple_spinner_dropdown_item);
                    dummyAdapterSemester.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
                    spinner_semester.setAdapter(dummyAdapterSemester);
                    dummyAdapterSemester.notifyDataSetChanged();
                    spinner_semester.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            semester = parent.getItemAtPosition(position).toString();
                        }
                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });

                }
                else if(degree.equalsIgnoreCase("MS"))
                {
                    //SEMESTER
                    ArrayAdapter<CharSequence> dummyAdapterSemester = ArrayAdapter.createFromResource(context, R.array.Semester4, R.layout.support_simple_spinner_dropdown_item);
                    dummyAdapterSemester.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
                    spinner_semester.setAdapter(dummyAdapterSemester);
                    dummyAdapterSemester.notifyDataSetChanged();
                    spinner_semester.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            semester = parent.getItemAtPosition(position).toString();
                        }
                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Shift
        ArrayAdapter<CharSequence> dummyAdapterShift = ArrayAdapter.createFromResource(this,R.array.Shift,R.layout.support_simple_spinner_dropdown_item);
        dummyAdapterShift.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinner_shift.setAdapter(dummyAdapterShift);

        spinner_shift.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                shift = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Section
        ArrayAdapter<CharSequence> dummyAdapterSection = ArrayAdapter.createFromResource(this,R.array.Section,R.layout.support_simple_spinner_dropdown_item);
        dummyAdapterSection.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinner_section.setAdapter(dummyAdapterSection);

        spinner_section.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                section = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Department
        ArrayAdapter<CharSequence> dummyAdapterDepartment = ArrayAdapter.createFromResource(this,R.array.Department,R.layout.support_simple_spinner_dropdown_item);
        dummyAdapterDepartment.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinner_dept.setAdapter(dummyAdapterDepartment);

        spinner_dept.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                department = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                rollNo = edt_rollno.getText().toString();
                name = edt_name.getText().toString();

                helper = new SqliteDatabaseHelper(getApplicationContext());
                helper.StudentinsertData(getApplicationContext(),new Student(rollNo,name,degree,semester,section,department,shift));

            }
        });
    }
}