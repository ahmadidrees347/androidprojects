package com.ahmad.attendence;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button btn_admin,btn_teacher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn_admin = findViewById(R.id.btn_admin);
        btn_teacher = findViewById(R.id.btn_teacher);

        btn_admin.setOnClickListener(this);
        btn_teacher.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btn_admin:
            {
                Intent admin = new Intent(getApplicationContext(),Admin.class);
                startActivity(admin);
                break;
            }
            case R.id.btn_teacher:
            {
                Intent teacher = new Intent(getApplicationContext(),Teacher_Main.class);
                startActivity(teacher);
                break;
            }
        }
    }
}
