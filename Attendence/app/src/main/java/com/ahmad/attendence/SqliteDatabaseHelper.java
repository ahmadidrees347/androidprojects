package com.ahmad.attendence;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class SqliteDatabaseHelper extends SQLiteOpenHelper {

    private Context context;
    private static int DATABASE_VERSION =1;
    private static final String DATABASE_NAME = "Android.db";

    //Table Student
    private static final String STUDENT_TABLE_NAME = "student";
    private static final String PRIMARY_STUDENT_ID = "s_id";
    private static final String COLUMN_STUDENT_ROLLNO = "s_rollno";
    private static final String COLUMN_STUDENT_DEGREE = "s_degree";
    private static final String COLUMN_STUDENT_NAME = "s_name";
    private static final String COLUMN_STUDENT_SEMESTER = "s_semester";
    private static final String COLUMN_STUDENT_SECTION = "s_section";
    private static final String COLUMN_STUDENT_DEPARTMENT = "s_department";
    private static final String COLUMN_STUDENT_SHIFT = "s_shift";

    static final String CREATE_STUDENT_TABLE = "CREATE TABLE IF NOT EXISTS " +
            STUDENT_TABLE_NAME + " ( " +
            PRIMARY_STUDENT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            COLUMN_STUDENT_ROLLNO + " TEXT, "+
            COLUMN_STUDENT_NAME + " TEXT, "+
            COLUMN_STUDENT_DEGREE + " TEXT, "+
            COLUMN_STUDENT_SEMESTER + " TEXT, "+
            COLUMN_STUDENT_SECTION + " TEXT, " +
            COLUMN_STUDENT_DEPARTMENT + " TEXT, " +
            COLUMN_STUDENT_SHIFT + " TEXT "
            + ")";

    //Table Teacher Dummy for Only Approval
    private static final String TEACHER_DUMMY_TABLE_NAME = "teacher_dummy";
    private static final String PRIMARY_TEACHER_ID_DUMMY = "td_id";
    private static final String COLUMN_TEACHER_NAME_DUMMY = "td_name";
    private static final String COLUMN_TEACHER_CNIC_DUMMY = "td_cnic";
    private static final String COLUMN_TEACHER_EMAIL_DUMMY = "td_email";
    private static final String COLUMN_TEACHER_PASSWORD_DUMMY = "td_password";

    static final String CREATE_TEACHER_TABLE_DUMMY = "CREATE TABLE IF NOT EXISTS " +
            TEACHER_DUMMY_TABLE_NAME + " ( " +
            PRIMARY_TEACHER_ID_DUMMY + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            COLUMN_TEACHER_NAME_DUMMY + " TEXT, "+
            COLUMN_TEACHER_CNIC_DUMMY + " TEXT, "+
            COLUMN_TEACHER_EMAIL_DUMMY + " TEXT, " +
            COLUMN_TEACHER_PASSWORD_DUMMY + " TEXT "
            + ")";

    //Table Teacher
    private static final String TEACHER_TABLE_NAME = "teacher";
    private static final String PRIMARY_T_ID = "t_id";
    private static final String COLUMN_T_NAME = "t_name";
    private static final String COLUMN_T_CNIC = "t_cnic";
    private static final String COLUMN_T_EMAIL = "t_email";
    private static final String COLUMN_T_PASSWORD = "t_password";

    static final String CREATE_TEACHER_TABLE = "CREATE TABLE IF NOT EXISTS " +
            TEACHER_TABLE_NAME + " ( " +
            PRIMARY_T_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            COLUMN_T_NAME + " TEXT, "+
            COLUMN_T_CNIC + " TEXT, "+
            COLUMN_T_EMAIL + " TEXT, " +
            COLUMN_T_PASSWORD + " TEXT "
            + ")";


    public SqliteDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(CREATE_STUDENT_TABLE);
        db.execSQL(CREATE_TEACHER_TABLE);
        db.execSQL(CREATE_TEACHER_TABLE_DUMMY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //create table again
        onCreate(db);

        //Drop older table if exists
        db.execSQL(String.format(" DROP TABLE IF EXISTS %s", CREATE_STUDENT_TABLE));
        db.execSQL(String.format(" DROP TABLE IF EXISTS %s", CREATE_TEACHER_TABLE));
        db.execSQL(String.format(" DROP TABLE IF EXISTS %s", CREATE_TEACHER_TABLE_DUMMY));

    }

    public List<Teacher> showAllRecordsofTeacher()
    {
        int id;
        String name,cnic,username, pswd;
        List<Teacher> teacherList = new ArrayList<>();
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        try
        {
            Cursor cursor = sqLiteDatabase.rawQuery("Select * from teacher_dummy",null);
            if(cursor!=null)
            {
                if(cursor.moveToFirst())
                {
                    do {
                        id = cursor.getInt(0);
                        name = cursor.getString(1);
                        cnic = cursor.getString(2);
                        username = cursor.getString(3);
                        pswd = cursor.getString(4);

                        teacherList.add(new Teacher(id,name,cnic,username,pswd));

                    }while(cursor.moveToNext());
                }
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        finally {
            sqLiteDatabase.close();
        }
        return teacherList;
    }

    public List<Student> showAllRecordsofStudents()
    {
        int id;
        String rollno, name, degree, semester, section, department, shift;
        List<Student> studentList = new ArrayList<>();
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        try
        {
            Cursor cursor = sqLiteDatabase.rawQuery("Select * from student",null);
            if(cursor!=null)
            {
                if(cursor.moveToFirst())
                {
                    do {
                        id = cursor.getInt(0);
                        rollno = cursor.getString(1);
                        name = cursor.getString(2);
                        degree = cursor.getString(3);
                        semester = cursor.getString(4);
                        section = cursor.getString(5);
                        department = cursor.getString(6);
                        shift = cursor.getString(7);

                        studentList.add(new Student(id,rollno,name,degree,semester,section,department,shift));

                    }while(cursor.moveToNext());
                }
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        finally {
            sqLiteDatabase.close();
        }
        return studentList;
    }

    public long StudentinsertData(Context context, Student student)
    {
        long result = 0;
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        try
        {
            values.put(COLUMN_STUDENT_ROLLNO,student.getRollno());
            values.put(COLUMN_STUDENT_NAME,student.getName());
            values.put(COLUMN_STUDENT_DEGREE,student.getDegree());
            values.put(COLUMN_STUDENT_SEMESTER,student.getSemester());
            values.put(COLUMN_STUDENT_SECTION,student.getSection());
            values.put(COLUMN_STUDENT_DEPARTMENT,student.getDepartment());
            values.put(COLUMN_STUDENT_SHIFT,student.getShift());

            //Insert is buildIn Function
            result = sqLiteDatabase.insert(STUDENT_TABLE_NAME,null,values);

            if(result>0)
            {
                Toast.makeText(context, "Value Inserted!", Toast.LENGTH_SHORT).show();
            }
            else
            {
                Toast.makeText(context, "Value Not Inserted!", Toast.LENGTH_SHORT).show();
            }

        }
        catch (SQLException e)
        {
            e.printStackTrace();
            Toast.makeText(context, "SQL Error!", Toast.LENGTH_SHORT).show();
        }

        finally {
            sqLiteDatabase.close();
        }

        return  result;
    }

    public boolean TeacherLogIN(String Temail, String Tpassword)
    {
        String db_email, db_pswd;
        SQLiteDatabase sqLiteDatabase=this.getReadableDatabase();
        try{
            Cursor cursor=sqLiteDatabase.rawQuery(String.format("SELECT * FROM teacher WHERE t_email = ? " +
                    "AND t_password = ? "), new String[] {Temail, Tpassword});
            if(cursor!=null) {
                if (cursor.moveToFirst()) {
                    do {
                        db_email = cursor.getString(3);
                        db_pswd = cursor.getString(4);

                        if(db_email.equalsIgnoreCase(Temail) && db_pswd.equalsIgnoreCase(Tpassword))
                        {
                            return true;
                        }
                    } while (cursor.moveToNext());
                }
            }
        }catch (SQLException e)
        {
            e.printStackTrace();
        }
        finally {
            sqLiteDatabase.close();
        }

        return false;
    }

    public long TeacherDummyinsertData(Context context, Teacher teacher)
    {
        long result = 0;
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        try
        {
            values.put(COLUMN_TEACHER_NAME_DUMMY,teacher.getName());
            values.put(COLUMN_TEACHER_CNIC_DUMMY,teacher.getCnic());
            values.put(COLUMN_TEACHER_EMAIL_DUMMY,teacher.getEmail());
            values.put(COLUMN_TEACHER_PASSWORD_DUMMY,teacher.getPswd());

            //Insert is buildIn Function
            result = sqLiteDatabase.insert(TEACHER_DUMMY_TABLE_NAME,null,values);

            if(result>0)
            {
                Toast.makeText(context, "Value Inserted!", Toast.LENGTH_SHORT).show();
            }
            else
            {
                Toast.makeText(context, "Value Not Inserted!", Toast.LENGTH_SHORT).show();
            }

        }
        catch (SQLException e)
        {
            e.printStackTrace();
            Toast.makeText(context, "SQL Error!", Toast.LENGTH_SHORT).show();
        }

        finally {
            sqLiteDatabase.close();
        }

        return  result;
    }

    public long TeacherinsertData(Context context, Teacher teacher)
    {
        long result = 0;
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        try
        {
            values.put(COLUMN_T_NAME,teacher.getName());
            values.put(COLUMN_T_CNIC,teacher.getCnic());
            values.put(COLUMN_T_EMAIL,teacher.getEmail());
            values.put(COLUMN_T_PASSWORD,teacher.getPswd());

            //Insert is buildIn Function
            result = sqLiteDatabase.insert(TEACHER_TABLE_NAME,null,values);

            if(result>0)
            {
                Toast.makeText(context, "Value Inserted!", Toast.LENGTH_SHORT).show();
            }
            else
            {
                Toast.makeText(context, "Value Not Inserted!", Toast.LENGTH_SHORT).show();
            }

        }
        catch (SQLException e)
        {
            e.printStackTrace();
            Toast.makeText(context, "SQL Error!", Toast.LENGTH_SHORT).show();
        }

        finally {
            sqLiteDatabase.close();
        }

        return  result;
    }

    public boolean deleteRecordById(int pk)
    {
        int getID;
        boolean isDeleted = false;
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();

        try
        {
            Cursor cursor=sqLiteDatabase.rawQuery(String.format("SELECT * FROM teacher_dummy WHERE td_id = ? "),new String []{String.valueOf(pk)});
            if(cursor!=null)
            {
                if(cursor.moveToFirst())
                {
                    do {
                        getID = cursor.getInt(0);

                        if(getID == pk)
                        {
                            sqLiteDatabase.delete(TEACHER_DUMMY_TABLE_NAME,"td_id = ?",new String []{String.valueOf(pk)});
                            isDeleted=true;
                            break;
                        }
                        else
                        {
                            isDeleted=false;
                        }
                    }while(cursor.moveToNext());
                }
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        finally {
            sqLiteDatabase.close();
        }

        return isDeleted;
    }

    public List<Teacher> FetchTeacherRecord(int pk)
    {
        int id;
        String name ,cnic, email, password;
        List<Teacher> teacherList = new ArrayList<>();
        SQLiteDatabase sqLiteDatabase=this.getReadableDatabase();
        try{
            Cursor cursor=sqLiteDatabase.rawQuery(String.format("SELECT * FROM teacher_dummy WHERE td_id = ? "),new String []{String.valueOf(pk)});
            if(cursor!=null) {
                if (cursor.moveToFirst()) {
                    do {
                        id = cursor.getInt(0);
                        if(id == pk){
                            Teacher teacher = new Teacher();
                            teacherList.add(new Teacher(cursor.getString(1),
                                    cursor.getString(2),
                                    cursor.getString(3),
                                    cursor.getString(4)));
                            break;
                        }
                        name = cursor.getString(1);
                        cnic = cursor.getString(2);
                        email = cursor.getString(3);
                        password = cursor.getString(4);
                        teacherList.add(new Teacher(id, name, cnic,email, password));

                    } while (cursor.moveToNext());
                }
            }

        }catch (SQLException e)
        {
            e.printStackTrace();
        }
        finally {
            sqLiteDatabase.close();
        }

        return teacherList;
    }

}

