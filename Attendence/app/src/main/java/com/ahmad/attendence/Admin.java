package com.ahmad.attendence;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Admin extends AppCompatActivity {

    Button btn_logIn;
    EditText edt_admin, edt_pswd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);

        btn_logIn = findViewById(R.id.btn_SignIn);

        edt_admin = findViewById(R.id.admin);
        edt_pswd = findViewById(R.id.password);

        btn_logIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //if(edt_admin.getText().toString().equalsIgnoreCase("admin") && edt_pswd.getText().toString().equalsIgnoreCase("admin"))
                {
                    Intent admin = new Intent(getApplicationContext(), AdminDashBoard.class);
                    startActivity(admin);
                }
                /*else
                {
                    Toast.makeText(Admin.this, "Try Again", Toast.LENGTH_SHORT).show();
                    edt_admin.setText("");
                    edt_pswd.setText("");
                }*/
            }
        });
    }
}
