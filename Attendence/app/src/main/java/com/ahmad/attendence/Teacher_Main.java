package com.ahmad.attendence;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Teacher_Main extends AppCompatActivity implements View.OnClickListener {

    Button TSignIn, TSignUp;
    EditText edt_temail, edt_tpswd;
    SqliteDatabaseHelper helper;
    TextView txt_email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher);

        txt_email = findViewById(R.id.txt_Temail);

        edt_temail = findViewById(R.id.T_email);
        edt_tpswd = findViewById(R.id.T_password);

        TSignIn = findViewById(R.id.btn_SignIn_teacher);
        TSignUp = findViewById(R.id.btn_SignUp_teacher);

        TSignIn.setOnClickListener(this);
        TSignUp.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btn_SignIn_teacher:
            {
                //If data Verify
                String email = edt_temail.getText().toString();
                String pswd = edt_tpswd.getText().toString();

                boolean check =false;
                helper =new SqliteDatabaseHelper(this);
                check = helper.TeacherLogIN(email, pswd);
                if (check == true){
                    Intent signIn = new Intent(getApplicationContext(),Teacher_Dashboard.class);
                    startActivity(signIn);
                    Toast.makeText(this, "record found", Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(this, "record not found", Toast.LENGTH_SHORT).show();
                }
                break;

            }
            case R.id.btn_SignUp_teacher:
            {
                Intent signUp = new Intent(getApplicationContext(),Teacher_Registeration.class);
                startActivity(signUp);
                break;
            }
        }
    }

}
