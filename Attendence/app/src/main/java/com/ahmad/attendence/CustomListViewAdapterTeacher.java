package com.ahmad.attendence;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class CustomListViewAdapterTeacher extends BaseAdapter {

    private Context context;
    private List<Teacher> teacherList;
    private LayoutInflater inflater;

    int primaryID;
    SqliteDatabaseHelper helper;


    public CustomListViewAdapterTeacher(Context context, List<Teacher> teacherList) {
        this.context = context;
        this.teacherList = teacherList;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount()
    {
        return teacherList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final Teacher teacher = teacherList.get(position);
        convertView = inflater.inflate(R.layout.activity_teacher_approvelist,parent,false);

        TextView txt_name = convertView.findViewById(R.id.txt_Tname);
        TextView txt_email = convertView.findViewById(R.id.txt_Temail);
        TextView txt_id = convertView.findViewById(R.id.srNo);
        ImageView img_Approve = convertView.findViewById(R.id.Approve);
        ImageView img_NotApprove = convertView.findViewById(R.id.NotApprove);

        txt_id.setText(String.valueOf(teacher.getId()));
        txt_name.setText(String.valueOf(teacher.getName()));
        txt_email.setText(teacher.getEmail());

        img_Approve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                primaryID = Integer.parseInt(String.valueOf(teacher.getId()));
                helper = new SqliteDatabaseHelper(context.getApplicationContext());
                List<Teacher> teacherListRecord;
                teacherListRecord = helper.FetchTeacherRecord(primaryID);
                boolean isDeleted;

                if (teacherListRecord !=null){
                    String name = "", cnic = "", email = "", pswd = "";
                    for (Teacher t : teacherListRecord){
                        name = t.getName();
                        cnic = t.getCnic();
                        email = t.getEmail();
                        pswd = t.getPswd();
                   }

                    helper.TeacherinsertData(context.getApplicationContext(),new Teacher(name,cnic,email,pswd));

                    isDeleted = helper.deleteRecordById(primaryID);

                    if(isDeleted)
                    {
                        teacherList.remove(position);
                        notifyDataSetChanged();
                        Toast.makeText(context, "DELETED", Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        Toast.makeText(context, "Not Deleted", Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });

        img_NotApprove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                primaryID = Integer.parseInt(String.valueOf(teacher.getId()));
                helper = new SqliteDatabaseHelper(context.getApplicationContext());
                boolean isDeleted;

                isDeleted = helper.deleteRecordById(primaryID);

                if(isDeleted)
                {
                    //Delete also from from screen in run time
                    teacherList.remove(position);
                    notifyDataSetChanged();
                    Toast.makeText(context, "DELETED", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Toast.makeText(context, "Not Deleted", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return convertView;
    }
}


