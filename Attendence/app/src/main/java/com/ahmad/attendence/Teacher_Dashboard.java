package com.ahmad.attendence;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Teacher_Dashboard extends AppCompatActivity implements View.OnClickListener {

    Button btn_V_attendance, btn_Add_attendence;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher__dashboard);

        btn_V_attendance = findViewById(R.id.btn_View_attendence);
        btn_Add_attendence = findViewById(R.id.btn_ADD_attendence);

        btn_V_attendance.setOnClickListener(this);
        btn_Add_attendence.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btn_View_attendence:
            {
                Intent view = new Intent(getApplicationContext(),ViewAttendence.class);
                startActivity(view);
                break;
            }
            case R.id.btn_ADD_attendence:
            {
                Intent add = new Intent(getApplicationContext(),AddAttendance.class);
                startActivity(add);
                break;
            }
        }
    }
}
