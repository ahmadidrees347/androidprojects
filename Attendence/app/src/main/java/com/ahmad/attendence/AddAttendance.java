package com.ahmad.attendence;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class AddAttendance extends AppCompatActivity {

    ListView listView;
    List<Student> studentList;
    SqliteDatabaseHelper sqliteDatabaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_attendance);

        listView = findViewById(R.id.Studentlist);

        studentList = new ArrayList<>();
        sqliteDatabaseHelper = new SqliteDatabaseHelper(this);
        studentList = sqliteDatabaseHelper.showAllRecordsofStudents();
        //Log.d("rsult",studentList.toString());
        CustomListViewAdapterStudent adapter = new CustomListViewAdapterStudent(this,studentList);
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }
}
