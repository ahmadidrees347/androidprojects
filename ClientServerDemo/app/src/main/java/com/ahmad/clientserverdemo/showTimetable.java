package com.ahmad.clientserverdemo;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class showTimetable extends AppCompatActivity {
    Spinner spnShift, spnSection, spnDay, spnDegree, spnDepartment, spnSemester;
    Button btn_timetable;
    Timetable<TimetableArray> timetableArray;
    private ProgressDialog progressDialog;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_timetable);

        recyclerView = findViewById(R.id.TimetablerecyleList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        spnShift = findViewById(R.id.spn_Shift);
        spnSection = findViewById(R.id.spn_Section);
        spnDay = findViewById(R.id.spn_Day);
        spnDegree = findViewById(R.id.spn_Degree);
        spnDepartment = findViewById(R.id.spn_Department);
        spnSemester = findViewById(R.id.spn_Semester);

        btn_timetable = findViewById(R.id.btn_timetable);
        btn_timetable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String shift = (String) spnShift.getSelectedItem();
                String section = (String) spnSection.getSelectedItem();
                String day = (String) spnDay.getSelectedItem();
                String degree = (String) spnDegree.getSelectedItem();
                String department = (String) spnDepartment.getSelectedItem();
                String semester = (String) spnSemester.getSelectedItem();

                showTimetable(shift, section, day, degree, department, semester);
            }
        });
    }
    private void showTimetable(String shift, String section, String day, String degree, String department, String semester)
    {
        timetableArray = new Timetable<>();
        TimetableServices timetableServices = RetrofitClient.getClient().create(TimetableServices.class);
        Call serviceCall = timetableServices.showTimetable(shift, section, day, degree, department, semester);
        serviceCall.enqueue(new Callback<Timetable<TimetableArray>>() {
            @Override
            public void onResponse(Call<Timetable<TimetableArray>> call, Response<Timetable<TimetableArray>> response) {
                if(response.isSuccessful())
                {
                    timetableArray = response.body();
                    if(!timetableArray.getError())
                    {
                        List<TimetableArray> timetableArrayList = timetableArray.getTimetableArray();

                        Timetable_Recycle_Adapter data= new Timetable_Recycle_Adapter(getApplicationContext(), timetableArrayList);
                        recyclerView.setAdapter(data);
                        //Toast.makeText(showSchedule.this, scheduleArray.getScheduleArray().get(0).getRName(), Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        Toast.makeText(showTimetable.this, timetableArray.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<Timetable<TimetableArray>> call, Throwable t) {
                Toast.makeText(showTimetable.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
