package com.ahmad.clientserverdemo;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class showSchedule extends AppCompatActivity {
    Spinner spinnerTeacher, spinnerDay;
    Button btn_schedule;
    ShowTeacherNames<TeacherArray> teacherArray;
    ScheduleData<ScheduleArray> scheduleArray;
    private ProgressDialog progressDialog;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_schedule);

        recyclerView = findViewById(R.id.recyleList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        spinnerTeacher = findViewById(R.id.spn_teacherName);
        spinnerDay = findViewById(R.id.spn_Day);
        btn_schedule = findViewById(R.id.btn_schedule);
        showTeacherName();
        btn_schedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String teacher = (String) spinnerTeacher.getSelectedItem();
                String day = (String) spinnerDay.getSelectedItem();
                showSchedule(teacher, day);
            }
        });
    }


    private void showSchedule(String name, String day)
    {
        scheduleArray = new ScheduleData<ScheduleArray>();
        ScheduleServices scheduleServices = RetrofitClient.getClient().create(ScheduleServices.class);
        Call serviceCall = scheduleServices.showSchedule(name, day);
        serviceCall.enqueue(new Callback<ScheduleData<ScheduleArray>>() {
            @Override
            public void onResponse(Call<ScheduleData<ScheduleArray>> call, Response<ScheduleData<ScheduleArray>> response) {
                if(response.isSuccessful())
                {
                    scheduleArray = response.body();
                    if(!scheduleArray.getError())
                    {
                        List<ScheduleArray> scheduleArrayList = scheduleArray.getScheduleArray();

                        Data_Recyle_Adapter data= new Data_Recyle_Adapter(getApplicationContext(), scheduleArrayList);
                        recyclerView.setAdapter(data);
                        Toast.makeText(showSchedule.this, scheduleArray.getScheduleArray().get(0).getRName(), Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        Toast.makeText(showSchedule.this, scheduleArray.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<ScheduleData<ScheduleArray>> call, Throwable t) {
                Toast.makeText(showSchedule.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showTeacherName()
    {
        teacherArray = new ShowTeacherNames<>();

        progressDialog = new ProgressDialog(showSchedule.this);
        progressDialog.setTitle("Get Result Plz Wait...");
        progressDialog.show();
        TeaherNamesServices teaherNamesServices = RetrofitClient.getClient().create(TeaherNamesServices.class);
        Call serviceCall = teaherNamesServices.showTeacherName();
        serviceCall.enqueue(new Callback<ShowTeacherNames<TeacherArray>>() {
            @Override
            public void onResponse(Call<ShowTeacherNames<TeacherArray>> call, Response<ShowTeacherNames<TeacherArray>> response) {
                if(response.isSuccessful())
                {
                    progressDialog.dismiss();
                    teacherArray = response.body();

                    if(!teacherArray.getError())
                    {
                        List<TeacherArray> teacherArrayList = teacherArray.getTeacherArray();
                        //Toast.makeText(showTeacher.this, teacherArrayList.get(0).getFName(), Toast.LENGTH_SHORT).show();

                        List<String> spnTeacherArray = new ArrayList<>();
                        spnTeacherArray.add("");
                        for(int i=0;i<teacherArrayList.size();i++)
                        {
                            spnTeacherArray.add(teacherArrayList.get(i).getFName());
                        }
                        try
                        {
                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(showSchedule.this,android.R.layout.simple_spinner_item,
                                    spnTeacherArray);
                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spinnerTeacher.setAdapter(adapter);
                        }
                        catch (Exception e)
                        {
                            Toast.makeText(showSchedule.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                    else
                    {
                        Toast.makeText(showSchedule.this, teacherArray.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    Toast.makeText(showSchedule.this, teacherArray.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ShowTeacherNames<TeacherArray>> call, Throwable t) {
                Toast.makeText(showSchedule.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }
}
