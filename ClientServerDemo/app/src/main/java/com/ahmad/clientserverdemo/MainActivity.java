package com.ahmad.clientserverdemo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    Button btn_schedule;
    Button btn_showTeacher, btn_timetable;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn_schedule = findViewById(R.id.btn_schedule);
        btn_showTeacher = findViewById(R.id.btn_showTeacher);
        btn_timetable = findViewById(R.id.btn_timetable);

        btn_schedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent schedule = new Intent(getApplicationContext(), showSchedule.class);
                startActivity(schedule);

            }
        });

        btn_showTeacher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent showTeacher = new Intent(getApplicationContext(), showTeacher.class);
                startActivity(showTeacher);

            }
        });

        btn_timetable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent showtimetable = new Intent(getApplicationContext(), showTimetable.class);
                startActivity(showtimetable);

            }
        });
    }
}
