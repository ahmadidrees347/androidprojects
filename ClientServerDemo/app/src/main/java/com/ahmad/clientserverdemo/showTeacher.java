package com.ahmad.clientserverdemo;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class showTeacher extends AppCompatActivity {
    Spinner spinnerTeacher;
    Button btn_showStatus;
    TextView txtStatus;
    static ShowTeacherNames<TeacherArray> teacherArray;
    TeacherStatus teacherStatus;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_teacher);

        showTeacherName();

        spinnerTeacher = findViewById(R.id.spn_teacherName);
        btn_showStatus = findViewById(R.id.btn_status);
        txtStatus = findViewById(R.id.txt_status);

        btn_showStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String teacher = (String) spinnerTeacher.getSelectedItem();
                showStatus(teacher);
                //Toast.makeText(showTeacher.this, item, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showStatus(String name)
    {
        teacherStatus = new TeacherStatus();
        //Toast.makeText(this, ""+name, Toast.LENGTH_SHORT).show();
        TeacherStatusServices teacherStatusServices = RetrofitClient.getClient().create(TeacherStatusServices.class);
        Call serviceCall = teacherStatusServices.showStatus(name);
        serviceCall.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                if(response.isSuccessful())
                {
                    TeacherStatus teacherStatus = (TeacherStatus) response.body();
                    if(!teacherStatus.getError())
                    {
                        txtStatus.setText(teacherStatus.getStatus());
                        //Toast.makeText(showTeacher.this, teacherStatus.getStatus(), Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        Toast.makeText(showTeacher.this, teacherStatus.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Toast.makeText(showTeacher.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showTeacherName()
    {
        teacherArray = new ShowTeacherNames<>();

        progressDialog = new ProgressDialog(showTeacher.this);
        progressDialog.setTitle("Get Result Plz Wait...");
        progressDialog.show();
        TeaherNamesServices teaherNamesServices = RetrofitClient.getClient().create(TeaherNamesServices.class);
        Call serviceCall = teaherNamesServices.showTeacherName();
        serviceCall.enqueue(new Callback<ShowTeacherNames<TeacherArray>>() {
            @Override
            public void onResponse(Call<ShowTeacherNames<TeacherArray>> call, Response<ShowTeacherNames<TeacherArray>> response) {
                if(response.isSuccessful())
                {
                    progressDialog.dismiss();
                    teacherArray = response.body();

                    if(!teacherArray.getError())
                    {
                        List<TeacherArray> teacherArrayList = teacherArray.getTeacherArray();
                        //Toast.makeText(showTeacher.this, teacherArrayList.get(0).getFName(), Toast.LENGTH_SHORT).show();

                        List<String> spnTeacherArray = new ArrayList<>();
                        spnTeacherArray.add("");
                        for(int i=0;i<teacherArrayList.size();i++)
                        {
                            spnTeacherArray.add(teacherArrayList.get(i).getFName());
                        }
                        try
                        {
                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(showTeacher.this,android.R.layout.simple_spinner_item,
                                    spnTeacherArray);
                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spinnerTeacher.setAdapter(adapter);
                        }
                        catch (Exception e)
                        {
                            Toast.makeText(showTeacher.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                    else
                    {
                        Toast.makeText(showTeacher.this, teacherArray.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    Toast.makeText(showTeacher.this, teacherArray.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ShowTeacherNames<TeacherArray>> call, Throwable t) {
                Toast.makeText(showTeacher.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }
}
