package com.ahmad.viewpagerdemo;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class SlideAdapter extends PagerAdapter {

    Context context;
    LayoutInflater inflater;

    int images[] = {
            R.drawable.image_1,
            R.drawable.image_2,
            R.drawable.image_3,
            R.drawable.image_4
    };

    String title[] = {
            "Comsmos","Satellite","Galaxy","Rocket"
    };

    int backgroundColor[] = {
            Color.rgb(55,55,55),
            Color.rgb(238,85,85),
            Color.rgb(110,49,89),
            Color.rgb(1,118,212)
    };

    public SlideAdapter(Context context)
    {
        this.context=context;
    }
    @Override
    public int getCount() {
        return title.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return (view == object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.slide,container,false);
        LinearLayout layout = view.findViewById(R.id.slide);
        ImageView imageView = view.findViewById(R.id.img);
        TextView textView = view.findViewById(R.id.title);

        layout.setBackgroundColor(backgroundColor[position]);
        imageView.setBackgroundResource(images[position]);
        textView.setText(title[position]);
        container.addView(view);

        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((LinearLayout) object);
    }
}
