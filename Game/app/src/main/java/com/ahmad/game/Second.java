package com.ahmad.game;

import android.content.Intent;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;

import java.util.Locale;

public class Second extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = Second.class.getSimpleName();
    Button num;
    Button alpha;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);



        num = findViewById(R.id.numeric);
        alpha = findViewById(R.id.alphabet);

        num.setOnClickListener(this);
        alpha.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.numeric: {
                Intent third = new Intent(getApplicationContext(), Third_Numeric.class);
                startActivity(third);

                finish();
                break;
            }
            case R.id.alphabet: {
                Intent third = new Intent(getApplicationContext(), Third_Alphabet.class);
                startActivity(third);

                finish();
                break;
            }
        }
    }



}
