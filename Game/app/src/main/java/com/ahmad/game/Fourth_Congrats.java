package com.ahmad.game;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Fourth_Congrats extends AppCompatActivity implements View.OnClickListener {

    Button btn_exit;
    Button btn_playAgain;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fourth__congrats);

        btn_exit=findViewById(R.id.btn_Exit);
        btn_playAgain=findViewById(R.id.btn_play);

        btn_playAgain.setOnClickListener(this);
        btn_exit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId())
        {
            case R.id.btn_play:
            {
                Intent third = new Intent(getApplicationContext(), Third_Numeric.class);
                startActivity(third);

                finish();
                break;
            }
            case R.id.btn_Exit:
            {

                finish();
                break;
            }
        }
    }
    @Override
    public void onBackPressed() {
        //Finish this activity AND
        finish();
        //Go to second Activity
        Intent second = new Intent(getApplicationContext(),Second.class);
        startActivity(second);
    }
}
