package com.ahmad.game;

import android.content.Intent;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;
import java.util.Random;



public class Third_Numeric extends AppCompatActivity implements View.OnClickListener {

    Button btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8, btn9, btn10;
    int matchArray[];
    int count, countbtn;

    //For TextToSpeech
    TextToSpeech tospeak;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third__numeric);

        //Initialize the TextToSpeech object
        tospeak = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {

                if (status != TextToSpeech.ERROR) {
                    tospeak.setLanguage(Locale.US);
                }
            }
        });



        //To know how many time button press
        count = 0;
        countbtn = 0;
        //For matching the output which is entered by user
        matchArray = new int[10];
        for (int i = 0; i < 10; i++) {
            matchArray[i] = i;
        }

        //Get id of all buttons
        btn1 = findViewById(R.id.btn_One);
        btn2 = findViewById(R.id.btn_Two);
        btn3 = findViewById(R.id.btn_Three);
        btn4 = findViewById(R.id.btn_Four);
        btn5 = findViewById(R.id.btn_Five);
        btn6 = findViewById(R.id.btn_Six);
        btn7 = findViewById(R.id.btn_Seven);
        btn8 = findViewById(R.id.btn_Eight);
        btn9 = findViewById(R.id.btn_Nine);
        btn10 = findViewById(R.id.btn_Ten);

        //Random the button
        ArrayList<Integer> number = new ArrayList<Integer>();
        for (int i = 0; i < 10; i++)
            number.add(i);
        Collections.shuffle(number);

        //Display random numbers on buttons
        btn1.setText(Integer.toString(number.indexOf(0)));
        btn2.setText(Integer.toString(number.indexOf(1)));
        btn3.setText(Integer.toString(number.indexOf(2)));
        btn4.setText(Integer.toString(number.indexOf(3)));
        btn5.setText(Integer.toString(number.indexOf(4)));
        btn6.setText(Integer.toString(number.indexOf(5)));
        btn7.setText(Integer.toString(number.indexOf(6)));
        btn8.setText(Integer.toString(number.indexOf(7)));
        btn9.setText(Integer.toString(number.indexOf(8)));
        btn10.setText(Integer.toString(number.indexOf(9)));

        btn1.setOnClickListener(this);
        btn2.setOnClickListener(this);
        btn3.setOnClickListener(this);
        btn4.setOnClickListener(this);
        btn5.setOnClickListener(this);
        btn6.setOnClickListener(this);
        btn7.setOnClickListener(this);
        btn8.setOnClickListener(this);
        btn9.setOnClickListener(this);
        btn10.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        boolean check;

        switch (v.getId()) {
            case R.id.btn_One: {

                check = status(btn1);
                if (check == true) {
                    tospeak.speak(btn1.getText().toString(), TextToSpeech.QUEUE_FLUSH, null);
                    //To disable the Button
                    btn1.setEnabled(false);
                    //To Invisible the button
                    //btn1.setVisibility(View.INVISIBLE);
                    CountBtn();
                }
                else
                {
                    tospeak.speak("FALSE", TextToSpeech.QUEUE_FLUSH, null);
                }
                break;
            }
            case R.id.btn_Two: {

                check = status(btn2);
                if (check == true) {
                    tospeak.speak(btn2.getText().toString(), TextToSpeech.QUEUE_FLUSH, null);
                    btn2.setEnabled(false);
                    CountBtn();
                }
                else
                {
                    tospeak.speak("FALSE", TextToSpeech.QUEUE_FLUSH, null);
                }
                break;
            }
            case R.id.btn_Three: {
                tospeak.speak(btn3.getText().toString(), TextToSpeech.QUEUE_FLUSH, null);
                check = status(btn3);
                if (check == true) {
                    btn3.setEnabled(false);
                    CountBtn();
                }
                else
                {
                    tospeak.speak("FALSE", TextToSpeech.QUEUE_FLUSH, null);
                }
                break;
            }
            case R.id.btn_Four: {

                check = status(btn4);
                if (check == true) {
                    tospeak.speak(btn4.getText().toString(), TextToSpeech.QUEUE_FLUSH, null);
                    btn4.setEnabled(false);
                    CountBtn();
                }
                else
                {
                    tospeak.speak("FALSE", TextToSpeech.QUEUE_FLUSH, null);
                }
                break;
            }
            case R.id.btn_Five: {

                check = status(btn5);
                if (check == true) {
                    tospeak.speak(btn5.getText().toString(), TextToSpeech.QUEUE_FLUSH, null);
                    btn5.setEnabled(false);
                    CountBtn();
                }
                else
                {
                    tospeak.speak("FALSE", TextToSpeech.QUEUE_FLUSH, null);
                }
                break;
            }
            case R.id.btn_Six: {

                check = status(btn6);
                if (check == true) {
                    tospeak.speak(btn6.getText().toString(), TextToSpeech.QUEUE_FLUSH, null);
                    btn6.setEnabled(false);
                    CountBtn();
                }
                else
                {
                    tospeak.speak("FALSE", TextToSpeech.QUEUE_FLUSH, null);
                }
                break;
            }
            case R.id.btn_Seven: {

                check = status(btn7);
                if (check == true) {
                    tospeak.speak(btn7.getText().toString(), TextToSpeech.QUEUE_FLUSH, null);
                    btn7.setEnabled(false);
                    CountBtn();
                }
                else
                {
                    tospeak.speak("FALSE", TextToSpeech.QUEUE_FLUSH, null);
                }
                break;
            }
            case R.id.btn_Eight: {

                check = status(btn8);
                if (check == true) {
                    tospeak.speak(btn8.getText().toString(), TextToSpeech.QUEUE_FLUSH, null);
                    btn8.setEnabled(false);
                    CountBtn();
                }
                else
                {
                    tospeak.speak("FALSE", TextToSpeech.QUEUE_FLUSH, null);
                }
                break;
            }
            case R.id.btn_Nine: {

                check = status(btn9);
                if (check == true) {
                    tospeak.speak(btn9.getText().toString(), TextToSpeech.QUEUE_FLUSH, null);
                    btn9.setEnabled(false);
                    CountBtn();
                }
                else
                {
                    tospeak.speak("FALSE", TextToSpeech.QUEUE_FLUSH, null);
                }
                break;
            }
            case R.id.btn_Ten: {

                check = status(btn10);
                if (check == true) {
                    tospeak.speak(btn10.getText().toString(), TextToSpeech.QUEUE_FLUSH, null);
                    btn10.setEnabled(false);
                    CountBtn();
                }
                else
                {
                    tospeak.speak("FALSE", TextToSpeech.QUEUE_FLUSH, null);
                }
                break;
            }
        }
    }

    private void CountBtn() {
        countbtn++;
        if (countbtn == 10) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            tospeak.speak("Congratulation", TextToSpeech.QUEUE_FLUSH, null);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Toast.makeText(this, "Congratulation", Toast.LENGTH_SHORT).show();
            finish();

            Intent fourth = new Intent(getApplicationContext(), Fourth_Congrats.class);
            startActivity(fourth);
        }
    }

    private boolean status(Button id) {
        int a = matchArray[count];
        String str = Integer.toString(a);

        if (id.getText() == str) {
            count++;
            Toast.makeText(this, id.getText(), Toast.LENGTH_SHORT).show();
            return true;
        } else {
            Toast.makeText(this, "False", Toast.LENGTH_SHORT).show();
            return false;
        }

    }

    @Override
    public void onBackPressed() {
        //Finish this activity AND
        finish();
        //Go to second Activity
        Intent second = new Intent(getApplicationContext(),Second.class);
        startActivity(second);
    }
}