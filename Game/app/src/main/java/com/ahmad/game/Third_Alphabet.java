package com.ahmad.game;

import android.content.Intent;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;


public class Third_Alphabet extends AppCompatActivity implements View.OnClickListener {

    Button btn1,btn2,btn3,btn4,btn5,btn6,btn7,btn8,btn9,btn10;
    Button btn11,btn12,btn13,btn14,btn15,btn16,btn17,btn18,btn19,btn20;
    Button btn21,btn22,btn23,btn24,btn25,btn26;

    char alphabets[];
    int count,countbtn;
    //For TextToSpeech
    int result;
    TextToSpeech tospeak;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third__alphabet);


        //Initialize the TextToSpeech object
        tospeak= new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status)
            {
                if (status == TextToSpeech.SUCCESS)
                {
                    result = tospeak.setLanguage(Locale.UK);
                }
            }
        });


        //To know how many time button press
        count=0;
        countbtn=0;
        //For matching the output which is entered by user
        alphabets = new char[26];
        int index = 0;
        for (char c = 'A'; c <= 'Z'; c++) {
            alphabets[index++] = c;
        }

        //Get id of all buttons
        btn1=findViewById(R.id.btn_One);
        btn2=findViewById(R.id.btn_Two);
        btn3=findViewById(R.id.btn_Three);
        btn4=findViewById(R.id.btn_Four);
        btn5=findViewById(R.id.btn_Five);
        btn6=findViewById(R.id.btn_Six);
        btn7=findViewById(R.id.btn_Seven);
        btn8=findViewById(R.id.btn_Eight);
        btn9=findViewById(R.id.btn_Nine);
        btn10=findViewById(R.id.btn_Ten);
        btn11=findViewById(R.id.btn_Eleven);
        btn12=findViewById(R.id.btn_Tweleve);
        btn13=findViewById(R.id.btn_Thirteen);
        btn14=findViewById(R.id.btn_Fourteen);
        btn15=findViewById(R.id.btn_Fifteen);
        btn16=findViewById(R.id.btn_Sixteen);
        btn17=findViewById(R.id.btn_Seventeen);
        btn18=findViewById(R.id.btn_Eighteen);
        btn19=findViewById(R.id.btn_Ninteen);
        btn20=findViewById(R.id.btn_Twenty);
        btn21=findViewById(R.id.btn_TwentyOne);
        btn22=findViewById(R.id.btn_TwentyTwo);
        btn23=findViewById(R.id.btn_TwentyThree);
        btn24=findViewById(R.id.btn_TwentyFour);
        btn25=findViewById(R.id.btn_TwentyFive);
        btn26=findViewById(R.id.btn_TwentySix);

        //Ramdom the button
        ArrayList<Character> number = new ArrayList<Character>();
        char s;
        for (int i=0; i<26; i++)
        {
            s=alphabets[i];
            number.add(s);

           // Toast.makeText(this, Character.toString(number.get(i)), Toast.LENGTH_SHORT).show();
        }

        Collections.shuffle(number);

        String str = new String(String.valueOf(number));


        char a=str.charAt(1);
        String string=Character.toString(number.get(0));
        //Display random numbers on buttons
        btn1.setText(Character.toString(number.get(0)));
        btn2.setText(Character.toString(number.get(1)));
        btn3.setText(Character.toString(number.get(2)));
        btn4.setText(Character.toString(number.get(3)));
        btn5.setText(Character.toString(number.get(4)));
        btn6.setText(Character.toString(number.get(5)));
        btn7.setText(Character.toString(number.get(6)));
        btn8.setText(Character.toString(number.get(7)));
        btn9.setText(Character.toString(number.get(8)));
        btn10.setText(Character.toString(number.get(9)));
        btn11.setText(Character.toString(number.get(10)));
        btn12.setText(Character.toString(number.get(11)));
        btn13.setText(Character.toString(number.get(12)));
        btn14.setText(Character.toString(number.get(13)));
        btn15.setText(Character.toString(number.get(14)));
        btn16.setText(Character.toString(number.get(15)));
        btn17.setText(Character.toString(number.get(16)));
        btn18.setText(Character.toString(number.get(17)));
        btn19.setText(Character.toString(number.get(18)));
        btn20.setText(Character.toString(number.get(19)));
        btn21.setText(Character.toString(number.get(20)));
        btn22.setText(Character.toString(number.get(21)));
        btn23.setText(Character.toString(number.get(22)));
        btn24.setText(Character.toString(number.get(23)));
        btn25.setText(Character.toString(number.get(24)));
        btn26.setText(Character.toString(number.get(25)));

        btn1.setOnClickListener(this);
        btn2.setOnClickListener(this);
        btn3.setOnClickListener(this);
        btn4.setOnClickListener(this);
        btn5.setOnClickListener(this);
        btn6.setOnClickListener(this);
        btn7.setOnClickListener(this);
        btn8.setOnClickListener(this);
        btn9.setOnClickListener(this);
        btn10.setOnClickListener(this);
        btn11.setOnClickListener(this);
        btn12.setOnClickListener(this);
        btn13.setOnClickListener(this);
        btn14.setOnClickListener(this);
        btn15.setOnClickListener(this);
        btn16.setOnClickListener(this);
        btn17.setOnClickListener(this);
        btn18.setOnClickListener(this);
        btn19.setOnClickListener(this);
        btn20.setOnClickListener(this);
        btn21.setOnClickListener(this);
        btn22.setOnClickListener(this);
        btn23.setOnClickListener(this);
        btn24.setOnClickListener(this);
        btn25.setOnClickListener(this);
        btn26.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {

        boolean check;
        switch (v.getId())
        {
            case R.id.btn_One:
            {

                check=status(btn1);
                if(check==true)
                {
                    tospeak.speak(btn1.getText().toString(),TextToSpeech.QUEUE_FLUSH,null);
                    btn1.setEnabled(false);
                    CountBtn();
                }
                else
                {
                    tospeak.speak("FALSE", TextToSpeech.QUEUE_FLUSH, null);
                }
                break;
            }
            case R.id.btn_Two:
            {

                check=status(btn2);
                if(check==true)
                {
                    tospeak.speak(btn2.getText().toString(),TextToSpeech.QUEUE_FLUSH,null);
                    btn2.setEnabled(false);
                    CountBtn();
                }
                else
                {
                    tospeak.speak("FALSE", TextToSpeech.QUEUE_FLUSH, null);
                }
                break;
            }
            case R.id.btn_Three:
            {

                check=status(btn3);
                if(check==true)
                {
                    tospeak.speak(btn3.getText().toString(),TextToSpeech.QUEUE_FLUSH,null);
                    btn3.setEnabled(false);
                    CountBtn();
                }
                else
                {
                    tospeak.speak("FALSE", TextToSpeech.QUEUE_FLUSH, null);
                }
                break;
            }
            case R.id.btn_Four:
            {

                check=status(btn4);
                if(check==true)
                {
                    tospeak.speak(btn4.getText().toString(),TextToSpeech.QUEUE_FLUSH,null);
                    btn4.setEnabled(false);
                    CountBtn();
                }
                else
                {
                    tospeak.speak("FALSE", TextToSpeech.QUEUE_FLUSH, null);
                }
                break;
            }
            case R.id.btn_Five:
            {

                check=status(btn5);
                if(check==true)
                {
                    tospeak.speak(btn5.getText().toString(),TextToSpeech.QUEUE_FLUSH,null);
                    btn5.setEnabled(false);
                    CountBtn();
                }
                else
                {
                    tospeak.speak("FALSE", TextToSpeech.QUEUE_FLUSH, null);
                }
                break;
            }
            case R.id.btn_Six:
            {

                check=status(btn6);
                if(check==true)
                {
                    tospeak.speak(btn6.getText().toString(),TextToSpeech.QUEUE_FLUSH,null);
                    btn6.setEnabled(false);
                    CountBtn();
                }
                else
                {
                    tospeak.speak("FALSE", TextToSpeech.QUEUE_FLUSH, null);
                }
                break;
            }
            case R.id.btn_Seven:
            {

                check=status(btn7);
                if(check==true)
                {
                    tospeak.speak(btn7.getText().toString(),TextToSpeech.QUEUE_FLUSH,null);
                    btn7.setEnabled(false);
                    CountBtn();
                }
                else
                {
                    tospeak.speak("FALSE", TextToSpeech.QUEUE_FLUSH, null);
                }
                break;
            }
            case R.id.btn_Eight:
            {

                check=status(btn8);
                if(check==true)
                {
                    tospeak.speak(btn8.getText().toString(),TextToSpeech.QUEUE_FLUSH,null);
                    btn8.setEnabled(false);
                    CountBtn();
                }
                else
                {
                    tospeak.speak("FALSE", TextToSpeech.QUEUE_FLUSH, null);
                }
                break;
            }
            case R.id.btn_Nine:
            {

                check=status(btn9);
                if(check==true)
                {
                    tospeak.speak(btn9.getText().toString(),TextToSpeech.QUEUE_FLUSH,null);
                    btn9.setEnabled(false);
                    CountBtn();
                }
                else
                {
                    tospeak.speak("FALSE", TextToSpeech.QUEUE_FLUSH, null);
                }
                break;
            }
            case R.id.btn_Ten:
            {

                check=status(btn10);
                if(check==true)
                {
                    tospeak.speak(btn10.getText().toString(),TextToSpeech.QUEUE_FLUSH,null);
                    btn10.setEnabled(false);
                    CountBtn();
                }
                else
                {
                    tospeak.speak("FALSE", TextToSpeech.QUEUE_FLUSH, null);
                }
                break;
            }
            case R.id.btn_Eleven:
            {

                check=status(btn11);
                if(check==true)
                {
                    tospeak.speak(btn11.getText().toString(),TextToSpeech.QUEUE_FLUSH,null);
                    btn11.setEnabled(false);
                    CountBtn();
                }
                else
                {
                    tospeak.speak("FALSE", TextToSpeech.QUEUE_FLUSH, null);
                }
                break;
            }
            case R.id.btn_Tweleve:
            {

                check=status(btn12);
                if(check==true)
                {
                    tospeak.speak(btn12.getText().toString(),TextToSpeech.QUEUE_FLUSH,null);
                    btn12.setEnabled(false);
                    CountBtn();
                }
                else
                {
                    tospeak.speak("FALSE", TextToSpeech.QUEUE_FLUSH, null);
                }
                break;
            }
            case R.id.btn_Thirteen:
            {

                check=status(btn13);
                if(check==true)
                {
                    tospeak.speak(btn13.getText().toString(),TextToSpeech.QUEUE_FLUSH,null);
                    btn13.setEnabled(false);
                    CountBtn();
                }
                else
                {
                    tospeak.speak("FALSE", TextToSpeech.QUEUE_FLUSH, null);
                }
                break;
            }
            case R.id.btn_Fourteen:
            {

                check=status(btn14);
                if(check==true)
                {
                    tospeak.speak(btn14.getText().toString(),TextToSpeech.QUEUE_FLUSH,null);
                    btn14.setEnabled(false);
                    CountBtn();
                }
                else
                {
                    tospeak.speak("FALSE", TextToSpeech.QUEUE_FLUSH, null);
                }
                break;
            }
            case R.id.btn_Fifteen:
            {

                check=status(btn15);
                if(check==true)
                {
                    tospeak.speak(btn15.getText().toString(),TextToSpeech.QUEUE_FLUSH,null);
                    btn15.setEnabled(false);
                    CountBtn();
                }
                else
                {
                    tospeak.speak("FALSE", TextToSpeech.QUEUE_FLUSH, null);
                }
                break;
            }
            case R.id.btn_Sixteen:
            {

                check=status(btn16);
                if(check==true)
                {
                    tospeak.speak(btn16.getText().toString(),TextToSpeech.QUEUE_FLUSH,null);
                    btn16.setEnabled(false);
                    CountBtn();
                }
                else
                {
                    tospeak.speak("FALSE", TextToSpeech.QUEUE_FLUSH, null);
                }
                break;
            }
            case R.id.btn_Seventeen:
            {

                check=status(btn17);
                if(check==true)
                {
                    tospeak.speak(btn17.getText().toString(),TextToSpeech.QUEUE_FLUSH,null);
                    btn17.setEnabled(false);
                    CountBtn();
                }
                else
                {
                    tospeak.speak("FALSE", TextToSpeech.QUEUE_FLUSH, null);
                }
                break;
            }
            case R.id.btn_Eighteen:
            {

                check=status(btn18);
                if(check==true)
                {
                    tospeak.speak(btn18.getText().toString(),TextToSpeech.QUEUE_FLUSH,null);
                    btn18.setEnabled(false);
                    CountBtn();
                }
                else
                {
                    tospeak.speak("FALSE", TextToSpeech.QUEUE_FLUSH, null);
                }
                break;
            }
            case R.id.btn_Ninteen:
            {

                check=status(btn19);
                if(check==true)
                {
                    tospeak.speak(btn19.getText().toString(),TextToSpeech.QUEUE_FLUSH,null);
                    btn19.setEnabled(false);
                    CountBtn();
                }
                else
                {
                    tospeak.speak("FALSE", TextToSpeech.QUEUE_FLUSH, null);
                }
                break;
            }
            case R.id.btn_Twenty:
            {

                check=status(btn20);
                if(check==true)
                {
                    tospeak.speak(btn20.getText().toString(),TextToSpeech.QUEUE_FLUSH,null);
                    btn20.setEnabled(false);
                    CountBtn();
                }
                else
                {
                    tospeak.speak("FALSE", TextToSpeech.QUEUE_FLUSH, null);
                }
                break;
            }
            case R.id.btn_TwentyOne:
            {

                check=status(btn21);
                if(check==true)
                {
                    tospeak.speak(btn21.getText().toString(),TextToSpeech.QUEUE_FLUSH,null);
                    btn21.setEnabled(false);
                    CountBtn();
                }
                else
                {
                    tospeak.speak("FALSE", TextToSpeech.QUEUE_FLUSH, null);
                }
                break;
            }
            case R.id.btn_TwentyTwo:
            {

                check=status(btn22);
                if(check==true)
                {
                    tospeak.speak(btn22.getText().toString(),TextToSpeech.QUEUE_FLUSH,null);
                    btn22.setEnabled(false);
                    CountBtn();
                }
                else
                {
                    tospeak.speak("FALSE", TextToSpeech.QUEUE_FLUSH, null);
                }
                break;
            }
            case R.id.btn_TwentyThree:
            {

                check=status(btn23);
                if(check==true)
                {
                    tospeak.speak(btn23.getText().toString(),TextToSpeech.QUEUE_FLUSH,null);
                    btn23.setEnabled(false);
                    CountBtn();
                }
                else
                {
                    tospeak.speak("FALSE", TextToSpeech.QUEUE_FLUSH, null);
                }
                break;
            }
            case R.id.btn_TwentyFour:
            {

                check=status(btn24);
                if(check==true)
                {
                    tospeak.speak(btn24.getText().toString(),TextToSpeech.QUEUE_FLUSH,null);
                    btn24.setEnabled(false);
                    CountBtn();
                }
                else
                {
                    tospeak.speak("FALSE", TextToSpeech.QUEUE_FLUSH, null);
                }
                break;
            }
            case R.id.btn_TwentyFive:
            {

                check=status(btn25);
                if(check==true)
                {
                    tospeak.speak(btn25.getText().toString(),TextToSpeech.QUEUE_FLUSH,null);
                    btn25.setEnabled(false);
                    CountBtn();
                }
                else
                {
                    tospeak.speak("FALSE", TextToSpeech.QUEUE_FLUSH, null);
                }
                break;
            }
            case R.id.btn_TwentySix:
            {

                check=status(btn26);
                if(check==true)
                {
                    tospeak.speak(btn26.getText().toString(),TextToSpeech.QUEUE_FLUSH,null);
                    btn26.setEnabled(false);
                    CountBtn();
                }
                else
                {
                    tospeak.speak("FALSE", TextToSpeech.QUEUE_FLUSH, null);
                }
                break;
            }
        }

    }

    private void CountBtn() {

        countbtn++;
        if(countbtn==26)
        {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            tospeak.speak("Congratulation", TextToSpeech.QUEUE_FLUSH, null);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Toast.makeText(this, "Congratulation", Toast.LENGTH_SHORT).show();
            finish();

            Intent second = new Intent(getApplicationContext(), Second.class);
            startActivity(second);
        }
    }


    private boolean status(Button id) {

        char a=alphabets[count];
        String str=Character.toString(a);
        String str1= (String) id.getText();

        if(str.equalsIgnoreCase(str1) == true)
        {
            count++;
            Toast.makeText(this, id.getText(), Toast.LENGTH_SHORT).show();
            return true;
        }
        else
        {
            Toast.makeText(this, "False", Toast.LENGTH_SHORT).show();
            return false;
        }

    }

    @Override
    public void onBackPressed() {
        //Finish this activity AND
        finish();
        //Go to second Activity
        Intent second = new Intent(getApplicationContext(),Second.class);
        startActivity(second);
    }
}

