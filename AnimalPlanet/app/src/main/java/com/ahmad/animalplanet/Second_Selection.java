package com.ahmad.animalplanet;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Second_Selection extends AppCompatActivity implements View.OnClickListener {
    Button btn_pet;
    Button btn_forest;
    MediaPlayer mediaPlayer;

    static boolean select=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second__selection);

        //For Full Screen View
        this.getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        );
        //Background Music
        mediaPlayer=MediaPlayer.create(getApplicationContext(),R.raw.bg_music);
        mediaPlayer.start();
        mediaPlayer.setLooping(true);


        btn_forest = findViewById(R.id.btn_forest);
        btn_pet = findViewById(R.id.btn_pet);

        btn_forest.setOnClickListener(this);
        btn_pet.setOnClickListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mediaPlayer.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mediaPlayer.start();
        mediaPlayer.start();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btn_forest:
            {
                select=true;
                Intent teach = new Intent(getApplicationContext(),Third_Teaching.class);
                startActivity(teach);
                break;
            }
            case R.id.btn_pet:
            {
                select=false;
                Intent teach = new Intent(getApplicationContext(),Third_Teaching.class);
                startActivity(teach);
                break;
            }
        }
    }
}
