package com.ahmad.animalplanet;

import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import static com.ahmad.animalplanet.Third_Teaching.forestImages;
import static com.ahmad.animalplanet.Third_Teaching.forestNames;

public class Fourth_Quiz extends AppCompatActivity implements View.OnClickListener {

    //For TextToSpeech
    TextToSpeech tospeak;

    List<DATA> datalist;
    DATA d;
    ImageView img1;
    ImageView img2;
    ImageView img3;
    ImageView img4;

    ImageView img_1;
    ImageView img_2;
    ImageView img_3;
    ImageView img_4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fourth__quiz);

        //For Full Screen View
        this.getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        );

        //Initialize the TextToSpeech object
        tospeak = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {

                if (status != TextToSpeech.ERROR) {
                    tospeak.setLanguage(Locale.US);
                }
            }
        });

        img1 = findViewById(R.id.image1);
        img2 = findViewById(R.id.image2);
        img3 = findViewById(R.id.image3);
        img4 = findViewById(R.id.image4);

        img_1 = findViewById(R.id.img1);
        img_2 = findViewById(R.id.img2);
        img_3 = findViewById(R.id.img3);
        img_4 = findViewById(R.id.img4);

        datalist = new ArrayList<>();
        for (int i = 0; i < forestNames.length - 1; i++) {
            d = new DATA(forestImages[i], forestNames[i]);
            datalist.add(d);
        }

        Collections.shuffle(datalist);


        Toast.makeText(this, "Where is " + datalist.get(0).getName().toString(), Toast.LENGTH_SHORT).show();

        img1.setBackgroundResource(datalist.get(0).getImageID());
        img2.setBackgroundResource(datalist.get(1).getImageID());
        img3.setBackgroundResource(datalist.get(2).getImageID());
        img4.setBackgroundResource(datalist.get(3).getImageID());

        img1.setOnClickListener(this);
        img2.setOnClickListener(this);
        img3.setOnClickListener(this);
        img4.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        //img_1.setBackgroundResource(R.drawable.right);

        if (v.getId() == datalist.get(0).getImageID()) {
            img_1.setBackgroundResource(R.drawable.right);
            /*img_2.setBackgroundResource(R.drawable.wrong);
            img_3.setBackgroundResource(R.drawable.wrong);
            img_4.setBackgroundResource(R.drawable.wrong);*/
        }

        if (v.getId() == datalist.get(1).getImageID()) {
            img_2.setBackgroundResource(R.drawable.wrong);
            img_3.setBackgroundResource(R.drawable.wrong);
            img_4.setBackgroundResource(R.drawable.wrong);
        }

        if (v.getId() == datalist.get(2).getImageID()) {
            img_2.setBackgroundResource(R.drawable.wrong);
            img_3.setBackgroundResource(R.drawable.wrong);
            img_4.setBackgroundResource(R.drawable.wrong);
        }

        if (v.getId() == datalist.get(3).getImageID()) {
            img_2.setBackgroundResource(R.drawable.wrong);
            img_3.setBackgroundResource(R.drawable.wrong);
            img_4.setBackgroundResource(R.drawable.wrong);
        }


    }
}
