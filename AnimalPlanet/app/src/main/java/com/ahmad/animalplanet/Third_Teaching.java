package com.ahmad.animalplanet;

import android.content.Intent;
import android.media.MediaPlayer;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.ahmad.animalplanet.Second_Selection.select;

public class Third_Teaching extends AppCompatActivity implements View.OnClickListener {

    MediaPlayer mp;

    ImageView imgView;
    Button btn_forw;
    Button btn_back;
    Button btn_quiz;

    static int[] forestImages={R.drawable.bear,R.drawable.deer
            ,R.drawable.fox,R.drawable.kangaroo
            ,R.drawable.lion,R.drawable.monkey
            ,R.drawable.panda
            ,R.drawable.raccoon//,R.drawable.rat
            ,R.drawable.squirrel,R.drawable.wolf};

    static int[] forestVoices={R.raw.bear,R.raw.deer
            ,R.raw.fox,R.raw.kangaroo
            ,R.raw.lion,R.raw.monkey
            ,R.raw.panda
            ,R.raw.raccoon//,R.raw.rat
            ,R.raw.squirrel,R.raw.wolf};

    static String[] forestNames= {"bear","deer","fox","kangaroo","lion","monkey","panda",/*"rat",*/"raccoon","squirrel","wolf"};

    ListView list_item;

    int[] petVoices={//R.raw.camel,
             R.raw.cat
            ,R.raw.chicken,R.raw.cow
            ,R.raw.dog,R.raw.donkey
            //,R.raw.duck
            ,R.raw.goat
            ,R.raw.goose,R.raw.hamster
            //,R.raw.hedgehog
            ,R.raw.horse,R.raw.lamb
            ,R.raw.ostrich,R.raw.pig
            ,R.raw.rabbit,R.raw.rooster
            ,R.raw.sheep,R.raw.turkey};

    int[] petImages={//R.drawable.camel,
            R.drawable.cat
            ,R.drawable.chicken,R.drawable.cow
            ,R.drawable.dog,R.drawable.donkey
            //,R.drawable.duck
            ,R.drawable.goat
            ,R.drawable.goose,R.drawable.hamster
            //,R.drawable.hedgehog
            ,R.drawable.horse,R.drawable.lamb
            ,R.drawable.ostrich,R.drawable.pig
            ,R.drawable.rabbit,R.drawable.rooster
            ,R.drawable.sheep,R.drawable.turkey};

    String[] petNames= {"cat","chicken","cow","dog","donkey","goat","goose","hamster","horse","lamb","ostrich",
            "pig","rabbit","rooster","sheep","turkey"};
    int i=-1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third__teaching);

        //For Full Screen View
        this.getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        );


        imgView = findViewById(R.id.imageView);

        setBackgroundImage();

        btn_back = findViewById(R.id.btn_backward);
        btn_forw = findViewById(R.id.btn_forward);
        btn_quiz = findViewById(R.id.btn_guess);

        btn_back.setOnClickListener(this);
        btn_forw.setOnClickListener(this);
        btn_quiz.setOnClickListener(this);
    }

    private void setBackgroundImage() {
        if(select==true)
        {
            imgView.setBackgroundResource(forestImages[++i]);
            mp=MediaPlayer.create(getApplicationContext(),forestVoices[i]);
            mp.start();
        }
        else
        {
            imgView.setBackgroundResource(petImages[++i]);
            mp=MediaPlayer.create(getApplicationContext(),petVoices[i]);
            mp.start();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btn_backward:
            {
                if(i>0)
                {
                    if(select==true)
                    {
                        mp.stop();
                        imgView.setBackgroundResource(forestImages[--i]);
                        mp=MediaPlayer.create(getApplicationContext(),forestVoices[i]);
                        mp.start();
                    }
                    else
                    {
                        mp.stop();
                        imgView.setBackgroundResource(petImages[--i]);
                        mp=MediaPlayer.create(getApplicationContext(),petVoices[i]);
                        mp.start();
                    }
                }
                break;
            }
            case R.id.btn_forward:
            {
                if(i<(forestImages.length-1) && select==true)
                {
                    mp.stop();
                    imgView.setBackgroundResource(forestImages[++i]);
                    mp=MediaPlayer.create(getApplicationContext(),forestVoices[i]);
                    mp.start();
                }
                else if(i<(petImages.length-1) && select==false)
                {
                    mp.stop();
                    imgView.setBackgroundResource(petImages[++i]);
                    mp=MediaPlayer.create(getApplicationContext(),petVoices[i]);
                    mp.start();
                }
                break;
            }
            case R.id.btn_guess:
            {
                Intent quiz = new Intent(getApplicationContext(),Fourth_Quiz.class);
                startActivity(quiz);
                finish();
            }
        }
    }
}
