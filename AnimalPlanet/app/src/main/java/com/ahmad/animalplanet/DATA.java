package com.ahmad.animalplanet;

public class DATA {
    private int ImageID;
    private String name;

    public int getImageID() {
        return ImageID;
    }

    public void setImageID(int imageID) {
        ImageID = imageID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DATA(int imageID, String name) {
        ImageID = imageID;
        this.name = name;
    }

}
