package com.abdullah.sqllight;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;
import android.content.Intent;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
Button btn1,btn2,btn3,btn4;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btn1=findViewById(R.id.btn_Add);
        btn2=findViewById(R.id.btn_delete);
        btn3=findViewById(R.id.btn_update);
        btn4.findViewById(R.id.btn_dis);

        btn1.setOnClickListener(this);
        btn2.setOnClickListener(this);
        btn3.setOnClickListener(this);
        btn4.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_Add:
                Intent intent1=new Intent(getApplicationContext(),add_record.class);
                startActivity(intent1);
                break;

            case R.id.btn_dis:
                Intent intent4=new Intent(getApplicationContext(),display_record.class);
                startActivity(intent4);
                break;

            case R.id.btn_delete:
                Intent intent2=new Intent(getApplicationContext(),delete_record.class);
                startActivity(intent2);
                break;

            case R.id.btn_update:
                Intent intent3=new Intent(getApplicationContext(),update_record.class);
                startActivity(intent3);
                break;

        }

    }
}
