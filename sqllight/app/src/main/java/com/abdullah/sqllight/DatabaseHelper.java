package com.abdullah.sqllight;

import android.content.ContentValues;
import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE = "Android.db";
    private static int DATABASE_VERSION = 1;
    private static final String TABLE_NAME = "student";
    private static final String PRIMARY_KEY = "id";
    private static final String COLUMN_NAME = "name";
    private static final String COLUMN_EMAIL = "email";
    private static final String COLUMN_PASSWORD = "password";

    final String CREATE_TABLE_USER = "CREATE TABLE " + TABLE_NAME + "("
            + PRIMARY_KEY + "INTEGER PRIMARY KEY AUTOINCREMENT,"
            + COLUMN_NAME + "Text"
            + COLUMN_EMAIL + "Text"
            + COLUMN_PASSWORD + "Text"+")";


    public long insertData(Context context,Student student)
    {
        long result = 0;
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        try{

            values.put(COLUMN_NAME,student.getName());
            values.put(COLUMN_EMAIL,student.getEmail());
            values.put(COLUMN_PASSWORD,student.getPassword());

           result = sqLiteDatabase.insert(TABLE_NAME,null,values);

           if (result>0)
           {
               Toast.makeText(context, "values inserted!", Toast.LENGTH_SHORT).show();
           }

           else {
               Toast.makeText(context, "values not inserted!", Toast.LENGTH_SHORT).show();
           }

        }

        catch (SQLException e)
        {
            e.printStackTrace();
            Toast.makeText(context, "sql error!", Toast.LENGTH_SHORT).show();
        }

        finally {
            sqLiteDatabase.close();
        }

        return result;
    }

    public DatabaseHelper(Context context){
        super(context, DATABASE, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_USER);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onCreate(db);
        db.execSQL("DROP TABLE IF EXISTS "+ TABLE_NAME );
    }
}
