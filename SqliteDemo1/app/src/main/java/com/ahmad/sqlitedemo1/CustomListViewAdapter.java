package com.ahmad.sqlitedemo1;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

//import com.squareup.picasso.Picasso;

import java.util.List;


public class CustomListViewAdapter extends BaseAdapter {

    private Context context;
    private List<Student> studentList;
    private LayoutInflater inflater;


    public CustomListViewAdapter(Context context, List<Student> studentList) {
        this.context = context;
        this.studentList = studentList;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount()
    {
        return studentList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final Student student = studentList.get(position);
        convertView = inflater.inflate(R.layout.activity_list,parent,false);

        TextView txt_id = convertView.findViewById(R.id.id);
        TextView txt_name = convertView.findViewById(R.id.name);
        TextView txt_email = convertView.findViewById(R.id.email);
        TextView txt_pswd = convertView.findViewById(R.id.pswd);

        txt_id.setText(String.valueOf(student.getId()));
        txt_name.setText(student.getUsername());
        txt_email.setText(student.getEmail());
        txt_pswd.setText(student.getPassword());




        return convertView;
    }
}

