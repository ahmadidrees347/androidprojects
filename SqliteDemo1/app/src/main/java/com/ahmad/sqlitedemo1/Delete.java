package com.ahmad.sqlitedemo1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Delete extends AppCompatActivity {

    Button del;
    EditText editText;
    int getId;
    boolean isDeleted;
    SqliteDatabaseHelper helper;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete);
        
        del = findViewById(R.id.delete);
        editText = findViewById(R.id.edt_pk);
        
        del.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                
                getId = Integer.parseInt(editText.getText().toString());
                helper = new SqliteDatabaseHelper(getApplicationContext());
                isDeleted = helper.deleteRecordById(getId);
                
                if(isDeleted)
                {
                    Toast.makeText(Delete.this, "DELETED", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Toast.makeText(Delete.this, "Not Found", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
