package com.ahmad.sqlitedemo1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements OnClickListener {
    Button btn_add,btn_del,btn_update,btn_view,btn_fetch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn_add=findViewById(R.id.btn_ADD);
        btn_del=findViewById(R.id.btn_DELETE);
        btn_update=findViewById(R.id.btn_UPDATE);
        btn_view = findViewById(R.id.btn_VIEW);
        btn_fetch = findViewById(R.id.btn_Fetch);

        btn_add.setOnClickListener(this);
        btn_del.setOnClickListener(this);
        btn_update.setOnClickListener(this);
        btn_view.setOnClickListener(this);
        btn_fetch.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btn_Fetch:
            {
                Intent fetch = new Intent(getApplicationContext(),Fetch_Record.class);
                startActivity(fetch);
                break;
            }
            case R.id.btn_ADD:
            {
                Intent add = new Intent(getApplicationContext(),ADD.class);
                startActivity(add);
                break;
            }
            case R.id.btn_DELETE:
            {
                Intent delete = new Intent(getApplicationContext(),Delete.class);
                startActivity(delete);
                break;
            }
            case R.id.btn_UPDATE:
            {
                Intent update = new Intent(getApplicationContext(),Update.class);
                startActivity(update);
                break;
            }
            case R.id.btn_VIEW:
            {
                Intent view = new Intent(getApplicationContext(),ListViewActivity.class);
                startActivity(view);
                break;
            }
        }
    }
}
