package com.ahmad.sqlitedemo1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class Fetch_Record extends AppCompatActivity {

    TextView txt_name,txt_email,txt_password;
    EditText edt_pk;

    Button btn_fetch_data;
    int getID;
    SqliteDatabaseHelper helper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fetch__record);
        edt_pk=findViewById(R.id.edt_pk);
        txt_email=findViewById(R.id.edt_email);
        txt_password=findViewById(R.id.edt_pswd);
        txt_name=findViewById(R.id.edt_name);
        btn_fetch_data=findViewById(R.id.fetch_btn);
        btn_fetch_data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getID=Integer.parseInt(edt_pk.getText().toString());

                List<Student> studentList;
                helper =new SqliteDatabaseHelper(getApplicationContext());
                studentList=helper.FetchSingleRecord(getID);
                if (studentList !=null){
                    for (Student s : studentList){
                        txt_name.setText(s.getUsername());
                        txt_email.setText(s.getEmail());
                        txt_password.setText(s.getPassword());
                    }
                }
                else {
                    Toast.makeText(Fetch_Record.this, "record not found", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}

