package com.ahmad.sqlitedemo1;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class Update extends AppCompatActivity {
    
    EditText edt_pk,edt_name,edt_email,edt_password;
    Button btn_fetch_data,btn_update;
    int getID;
    boolean check=false;
    SqliteDatabaseHelper helper;
    Context context= this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);
        edt_pk=findViewById(R.id.edt_pkey);
        edt_email=findViewById(R.id.edt_email);
        edt_password=findViewById(R.id.edt_pswd);
        edt_name=findViewById(R.id.edt_name);
        
        btn_fetch_data=findViewById(R.id.btn_show);
        btn_update=findViewById(R.id.btn_update);

        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(check==true)
                {
                    String username = edt_name.getText().toString();
                    String email = edt_email.getText().toString();
                    String pswd = edt_password.getText().toString();

                    helper = new SqliteDatabaseHelper(context);
                    Toast.makeText(context, "YES", Toast.LENGTH_SHORT).show();
                    helper.updateRecordById(getID,context,new Student(username,email,pswd));

                }
            }
        });

        btn_fetch_data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getID=Integer.parseInt(edt_pk.getText().toString());

                List<Student> studentList;
                helper =new SqliteDatabaseHelper(getApplicationContext());
                studentList=helper.FetchSingleRecord(getID);
                if (studentList !=null){
                    for (Student s : studentList){
                        edt_name.setText(s.getUsername());
                        edt_email.setText(s.getEmail());
                        edt_password.setText(s.getPassword());
                    }
                    check=true;
                }
                else {
                    Toast.makeText(Update.this, "Not Found", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}