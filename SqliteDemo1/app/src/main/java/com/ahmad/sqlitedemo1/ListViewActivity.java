package com.ahmad.sqlitedemo1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class ListViewActivity extends AppCompatActivity {

    ListView listView;
    List<Student> studentList;
    SqliteDatabaseHelper sqliteDatabaseHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view);

        listView = findViewById(R.id.listview);

        studentList = new ArrayList<>();
        sqliteDatabaseHelper = new SqliteDatabaseHelper(this);
        studentList = sqliteDatabaseHelper.showAllRecords();

        CustomListViewAdapter adapter = new CustomListViewAdapter(this,studentList);
        listView.setAdapter(adapter);


    }
}
