package com.ahmad.sqlitedemo1;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class SqliteDatabaseHelper extends SQLiteOpenHelper {

    private Context context;
    private static int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "Android.db";
    private static final String TABLE_NAME = "user";
    private static final String PRIMARY_ID = "id";
    private static final String COLUMN_USERNAME = "user_name";
    private static final String COLUMN_EMAIL = "email";
    private static final String COLUMN_PASSWORD = "password";

    static final String CREATE_USER_TABLE = "CREATE TABLE IF NOT EXISTS " +
            TABLE_NAME + " ( " +
            PRIMARY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            COLUMN_USERNAME + " TEXT, "+
            COLUMN_EMAIL + " TEXT, " +
            COLUMN_PASSWORD + " TEXT "
            + ")";


    public SqliteDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(CREATE_USER_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //create table again
        onCreate(db);

        //Drop older table if exists
        db.execSQL("DROP TABLE IF EXISTS "+ TABLE_NAME);
    }



    public List<Student> showAllRecords()
    {
        int id;
        String name,email,password;
        List<Student> studentList = new ArrayList<>();
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        try
        {
            Cursor cursor = sqLiteDatabase.rawQuery("Select * from user",null);
            if(cursor!=null)
            {
                if(cursor.moveToFirst())
                {
                    do {
                        id = cursor.getInt(0);
                        name = cursor.getString(1);
                        email = cursor.getString(2);
                        password = cursor.getString(3);

                        studentList.add(new Student(id,name,email,password));

                    }while(cursor.moveToNext());
                }
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        finally {
            sqLiteDatabase.close();
        }
        return studentList;
    }

    public long insertData(Context context, Student student)
    {
        long result = 0;
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        try
        {
            values.put(COLUMN_USERNAME,student.getUsername());
            values.put(COLUMN_EMAIL,student.getEmail());
            values.put(COLUMN_PASSWORD,student.getPassword());

            //Insert is buildIn Function
            result = sqLiteDatabase.insert(TABLE_NAME,null,values);

            if(result > 0)
            {
                Toast.makeText(context, "Value Inserted!", Toast.LENGTH_SHORT).show();
            }
            else
            {
                Toast.makeText(context, "Value Not Inserted!", Toast.LENGTH_SHORT).show();
            }

        }
        catch (SQLException e)
        {
            e.printStackTrace();
            Toast.makeText(context, "SQL Error!", Toast.LENGTH_SHORT).show();
        }

        finally {
            sqLiteDatabase.close();
        }

        return  result;
    }

    public List<Student> FetchSingleRecord(int pk)
    {
        int id;
        String name,email,password;
        List<Student> studentList=new ArrayList<>();
        SQLiteDatabase sqLiteDatabase=this.getReadableDatabase();
        try{
            Cursor cursor=sqLiteDatabase.rawQuery(String.format("SELECT * FROM user WHERE id = ? "),new String []{String.valueOf(pk)});
            if(cursor!=null) {
                if (cursor.moveToFirst()) {
                    do {
                        id = cursor.getInt(0);
                        if(id==pk){
                            Student student=new Student();
                            studentList.add(new Student(cursor.getString(1),
                                    cursor.getString(2),
                                    cursor.getString(3)));
                            break;
                        }
                        name = cursor.getString(1);
                        email = cursor.getString(2);
                        password = cursor.getString(3);
                        studentList.add(new Student(id, name, email, password));

                    } while (cursor.moveToNext());
                }
            }

        }catch (SQLException e)
        {
            e.printStackTrace();
        }
        finally {
            sqLiteDatabase.close();
        }

        return studentList;
    }
    public boolean deleteRecordById(int pk)
    {
        int getID;
        boolean isDeleted = false;
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();

        try
        {
            Cursor cursor=sqLiteDatabase.rawQuery(String.format("SELECT * FROM user WHERE id = ? "),new String []{String.valueOf(pk)});
            if(cursor!=null)
            {
                if(cursor.moveToFirst())
                {
                    do {
                        getID = cursor.getInt(0);

                        if(getID == pk)
                        {
                            sqLiteDatabase.delete(TABLE_NAME,"id = ?",new String []{String.valueOf(pk)});
                            isDeleted=true;
                            break;
                        }
                        else
                        {
                            isDeleted=false;
                        }
                    }while(cursor.moveToNext());
                }
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        finally {
            sqLiteDatabase.close();
        }

        return isDeleted;
    }
    public void updateRecordById(int pk,Context context, Student student)
    {
        int getID;
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();

        try
        {
            Cursor cursor=sqLiteDatabase.rawQuery(String.format("SELECT * FROM user WHERE id = ? "),new String []{String.valueOf(pk)});
            ContentValues values = new ContentValues();
            values.put(COLUMN_USERNAME,student.getUsername());
            values.put(COLUMN_EMAIL,student.getEmail());
            values.put(COLUMN_PASSWORD,student.getPassword());
            if(cursor!=null)
            {
                if(cursor.moveToFirst())
                {
                    do {
                        getID = cursor.getInt(0);

                        if(getID == pk)
                        {
                            sqLiteDatabase.update(TABLE_NAME,values,"id = ?",new String []{String.valueOf(pk)});

                            break;
                        }
                        else
                        {
                            Toast.makeText(context, "NO", Toast.LENGTH_SHORT).show();
                        }
                    }while(cursor.moveToNext());
                }
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        finally {
            sqLiteDatabase.close();
        }

    }




    /*public void insert(String username, String email, String password)
    {
        try
        {
            SQLiteDatabase database = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(COLUMN_USERNAME,username);
            values.put(COLUMN_EMAIL,email);
            values.put(COLUMN_PASSWORD,password);

            Log.d("Insertion","Completed");
            //Toast.makeText(context, "DATA Inserted", Toast.LENGTH_SHORT).show();

            database.insert(TABLE_NAME,null,values);
        }
        catch(SQLException e)
        {
            Toast.makeText(context, "Insertion Failed", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }

    }*/
}
