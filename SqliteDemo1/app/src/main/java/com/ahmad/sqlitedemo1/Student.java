package com.ahmad.sqlitedemo1;

public class Student {
    int id;
    String username;
    String email;
    String password;

    public Student()
    {    }
    /**
     * const: for fetch all record from db
     * @param id
     * @param username
     * @param email
     * @param password
     */

    public Student(int id, String username, String email, String password) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.password = password;
    }

    /**
     * const: for saving record
     * @param username -> userName {#link DatabaseHelper}
     * @param email
     * @param password
     */

    public Student(String username, String email, String password) {
        this.username = username;
        this.email = email;
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {

        return username;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }


}
