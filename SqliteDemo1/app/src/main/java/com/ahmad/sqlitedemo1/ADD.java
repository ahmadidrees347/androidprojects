package com.ahmad.sqlitedemo1;

import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

public class ADD extends AppCompatActivity {
    Button btn_save;
    EditText edt_name,edt_email,edt_pswd;

    SqliteDatabaseHelper helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        edt_name = findViewById(R.id.name);
        edt_email = findViewById(R.id.email);
        edt_pswd = findViewById(R.id.password);

        btn_save = findViewById(R.id.btn_save);
       btn_save.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               addRecord();
           }
       });
    }
    private void addRecord()
    {
        String username = edt_name.getText().toString();
        String email = edt_email.getText().toString();
        String pswd = edt_pswd.getText().toString();

        helper = new SqliteDatabaseHelper(this);
        helper.insertData(this,new Student(username,email,pswd));
    }


    /*private void insert()
    {
        username = edt_name.getText().toString();
        email = edt_email.getText().toString();
        pswd = edt_pswd.getText().toString();

        helper = new SqliteDatabaseHelper(this);
        //helper.insert(username,email,pswd);
    }*/

}
