package com.ahmad.allmeters;

import android.annotation.SuppressLint;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class VibrationMeter extends AppCompatActivity {
    Button btn_vib;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vibration_meter);

        btn_vib = findViewById(R.id.btn_vibration);

        final Vibrator vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);

        final long[] pattern = {2000, 1000};

        btn_vib.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("MissingPermission")
            @Override
            public void onClick(View view) {
                if(btn_vib.getText().toString().equals("Start"))
                {
                    vibrator.vibrate(pattern,0);
                    Toast.makeText(VibrationMeter.this, "Started", Toast.LENGTH_SHORT).show();
                    btn_vib.setText("Stop");
                }
                else
                {
                    vibrator.cancel();
                    btn_vib.setText("Start");
                    Toast.makeText(VibrationMeter.this, "Cancelled", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
