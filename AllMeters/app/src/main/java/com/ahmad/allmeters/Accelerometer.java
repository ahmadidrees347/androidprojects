package com.ahmad.allmeters;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import org.w3c.dom.Text;

public class Accelerometer extends AppCompatActivity implements SensorEventListener {

    TextView txt_X, txt_Y, txt_Z;
    Sensor sensor;
    SensorManager sensorManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accelerometer);

        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        sensor = sensorManager.getDefaultSensor(sensor.TYPE_ACCELEROMETER);
        sensorManager.registerListener(this,sensor,sensorManager.SENSOR_DELAY_NORMAL);

        txt_X = findViewById(R.id.X);
        txt_Y = findViewById(R.id.Y);
        txt_Z = findViewById(R.id.Z);

    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        txt_X.setText("X : " + sensorEvent.values[0]);
        txt_Y.setText("Y : " + sensorEvent.values[1]);
        txt_Z.setText("Z : " + sensorEvent.values[2]);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
}
