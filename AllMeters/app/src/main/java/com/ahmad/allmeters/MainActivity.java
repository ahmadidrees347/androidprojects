package com.ahmad.allmeters;

import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Button btn_step_counter;
    Button btn_accelerometer;
    Button btn_vibrationometer;
    Button btn_speedometer;
    Button btn_mfmeter;
    Button btn_soundmeter;
    Button btn_signalstrengthmeter;
    Button btn_internetmeter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn_step_counter = findViewById(R.id.step_counter);
        btn_accelerometer = findViewById(R.id.accelerometer);
        btn_vibrationometer = findViewById(R.id.vibrationMeter);
        btn_speedometer = findViewById(R.id.speedMeter);
        btn_mfmeter = findViewById(R.id.MFMeter);
        btn_soundmeter = findViewById(R.id.SoundMeter);
        btn_signalstrengthmeter = findViewById(R.id.SignalStengthMeter);
        btn_internetmeter = findViewById(R.id.InternetMeter);

        btn_step_counter.setOnClickListener(this);
        btn_accelerometer.setOnClickListener(this);
        btn_vibrationometer.setOnClickListener(this);
        btn_speedometer.setOnClickListener(this);
        btn_mfmeter.setOnClickListener(this);
        btn_soundmeter.setOnClickListener(this);
        btn_signalstrengthmeter.setOnClickListener(this);
        btn_internetmeter.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.step_counter:
            {
                Intent intent = new Intent(getApplicationContext(),Step_Counter.class);
                startActivity(intent);
                break;
            }
            case R.id.accelerometer:
            {
                Intent intent = new Intent(getApplicationContext(),Accelerometer.class);
                startActivity(intent);
                break;
            }
            case R.id.vibrationMeter:
            {
                Intent intent = new Intent(getApplicationContext(),VibrationMeter.class);
                startActivity(intent);
                break;
            }
            case R.id.speedMeter:
            {
                Intent intent = new Intent(getApplicationContext(),SpeedoMeter.class);
                startActivity(intent);
                break;
            }
            case R.id.MFMeter:
            {
                Intent intent = new Intent(getApplicationContext(),MagneticFeildMeter.class);
                startActivity(intent);
                break;
            }
            case R.id.SoundMeter:
            {
                Intent intent = new Intent(getApplicationContext(),SoundMeter.class);
                startActivity(intent);
                break;
            }
            case R.id.SignalStengthMeter:
            {
                Intent intent = new Intent(getApplicationContext(),SignalStrengthMeter.class);
                startActivity(intent);
                break;
            }
            case R.id.InternetMeter:
            {
                Intent intent = new Intent(getApplicationContext(),InternetSpeedMeter.class);
                startActivity(intent);
                break;
            }
        }
    }
}
