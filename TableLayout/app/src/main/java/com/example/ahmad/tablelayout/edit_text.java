package com.example.ahmad.tablelayout;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class edit_text extends AppCompatActivity
{
    Button btn_click;
    EditText edt_1,edt_2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.edittext);
        btn_click=findViewById(R.id.btn_edittext);
        edt_1=findViewById(R.id.edit_val_1);
        edt_2=findViewById(R.id.edit_val_2);

        btn_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                edt_1.setText("This is Android Class");
                btn_click.setText("Click ME!");
                
                String value = edt_2.getText().toString();
                Toast.makeText(edit_text.this, "value", Toast.LENGTH_SHORT).show();
            }
        });
    }


}
