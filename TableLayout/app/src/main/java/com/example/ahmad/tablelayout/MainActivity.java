package com.example.ahmad.tablelayout;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button btn1,edit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        btn1=(Button)findViewById(R.id.signup);
        edit=(Button)findViewById(R.id.btn_edittext);

        btn1.setOnClickListener(this);
        edit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.signup:
                Intent button = new Intent (MainActivity.this,btn_activity.class);
                startActivity(button);
                break;

            case R.id.btn_edittext:
                Toast.makeText(this,"Edittext",Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
