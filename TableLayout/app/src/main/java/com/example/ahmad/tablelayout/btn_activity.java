package com.example.ahmad.tablelayout;

import android.content.Intent;
import android.inputmethodservice.AbstractInputMethodService;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class btn_activity extends AppCompatActivity implements View.OnClickListener
{
    Button btn_button1,btn_button2,btn_button3;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        btn_button1=(Button)findViewById(R.id.btn_one);
        btn_button2=(Button)findViewById(R.id.btn_two);
        btn_button3=(Button)findViewById(R.id.btn_three);
        btn_button1.setOnClickListener(this);
        btn_button2.setOnClickListener(this);
        btn_button3.setOnClickListener(this);
    }
    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.btn_one:
                Toast.makeText(this,"Button1",Toast.LENGTH_SHORT).show();
                break;

            case R.id.btn_two:
                Toast.makeText(this,"Button2",Toast.LENGTH_SHORT).show();
                break;
            case R.id.btn_three:
                Toast.makeText(this,"Button3",Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
